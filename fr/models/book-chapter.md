[container]

[row]

[column width="3"]
[book-side-tree id="{id}"]
[/column]

[column width="9"]
{content}
[book-chapter-navigation id="{id}" text-previous="Précédent" text-next="Suivant"]
[/column]

[/row]
[/container]
