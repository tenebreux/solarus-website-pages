[container]

# Actualités

[row]
[column width="8"]
[article-listing read-more-label="Lire la suite..."]
[/column]

[column width="4"]
[box-highlight]

## Filtres

[entity-filter entity-type="article" filter="categories" title="Catégories"]
[entity-filter entity-type="article" filter="authors" title="Auteurs"]
[/box-highlight]
[/column]
[/row]
[/container]