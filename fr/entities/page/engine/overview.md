[container layout="small"]
[align type="center"]

# {title}

[hr width="10"]

## Un moteur de jeu 2D pour Action-RPG

[hr width="40"]

[space thickness="20"]

[youtube id="vt07FwzLo9A"]

[space thickness="30"]

Solarus a été spécifiquement conçu avec les Action-RPG cultes de l'ère 2D en tête, comme **The Legend of Zelda: A Link to the Past** et **Secret of Mana** sur Super Nintendo, ou **Soleil** sur Sega Megadrive/Genesis.

Le moteur est codé en **C++**, avec la bibliothèque **SDL** et un backend **OpenGL**. Les jeux faits avec Solarus sont appelés des **quêtes** et son fait en **Lua**.

Le moteur fait tous les **calculs lourds** (par exemple : tests de collisions) et les **opérations bas-niveau** comme dessiner à l'écran, animer les sprites et jouer du son.

En tant que créateur de quête, vous n'avez pas à implémenter ces algorithmes. Au contraire, vous aurez à définir la **logique du jeu**. Vos scripts Lua décrivent le comportement des ennemis, ou ce qui se passe quand vous appuyez sur un interrupteur sur une carte spécifique. Ils serviront aussi à implémenter l'écran-titre et le HUD (informations à l'écran).

Les deux parties (le moteur en C++ et les scripts en Lua) communiquent grâce à **l'API Lua de Solarus**. La communication fonctionne dans les deux sens : vous pouvez appeler des fonctions du moteur (par exemple : déplacer un personnage) et le moteur appelera vos propres fonctions (par exemple : être informé quand un ennemi est tué). Mais avant d'utiliser l'API Lua de Solarus, vous devez apprendre les bases du Lua (un langage simple et minimal, mais très puissant).

[space]

[row]
[column]

#### Core C++

![C++ logo](images/cpp_logo.png "C++ logo")

[/column]
[column]

#### API Lua

![Lua logo](images/lua_logo.png "Lua logo")

[/column]
[column]

#### Shaders

[space thickness="15"]
![OpenGL logo](images/opengl_logo.png "OpenGL logo")

[/column]
[/row]

[space thicknesss="30"]

[/align]
[/container]

[highlight]
[container]
[align type="center"]
[space thickness="10"]

## Des outils pour les joueurs et les développeurs

[hr width="20"]
[row]
[column]

### Pour les joueurs

[/column]
[column]

### Pour les créateurs de quêtes

[/column]
[/row]

[row]
[column]
![Launcher miniature](images/launcher_miniature.png "Launcher miniature")
[space thickness="20"]
![Launcher logo](images/launcher_logo.png "Launcher logo")
[/column]
[column]
![Quest editor miniature](images/quest_editor_miniature.png "Quest editor miniature")
[space thickness="20"]
![Quest editor logo](images/quest_editor_logo.png "Quest editor logo")
[/column]
[/row]

[row]
[column]
[space thickness="20"]
Un logiciel pour lancer vos jeux et gérer votre ludothèque Solarus.
[/column]
[column]
[space thickness="20"]
Un éditeur graphique tout-en-un pour créer votre jeu et modifier les maps, les sprites, les dialogues, les scripts Lua ou encore les shaders.
[/column]
[/row]
[space]
[/align]
[/container]

[space]

[container]
[row]
[column]
[image url="images/screenshot-quest-editor-01.png" alt="Screenshot 1"]
[/column]
[column]
[image url="images/screenshot-quest-editor-02.png" alt="Screenshot 2"]
[/column]
[column]
[image url="images/screenshot-quest-editor-03.png" alt="Screenshot 3"]
[/column]
[column]
[image url="images/screenshot-quest-editor-04.png" alt="Screenshot 4"]
[/column]
[/row]
[/container]

[/highlight]

[container]
[align type="center]
[space thickness="10"]

## Multiplatforme

[hr width="10"]

Solarus est disponible sur un grand nombre de systèmes, dont Windows, macOS, Linux, BSD et même Android (bientôt).

[space thickness="10"]

[row]
[column]
[icon icon="windows" category="fab" type="default" size="4x"]
[/column]
[column]
[icon icon="apple" category="fab" type="default" size="4x"]
[/column]
[column]
[icon icon="android" category="fab" type="default" size="4x"]
[/column]
[column]
[icon icon="ubuntu" category="fab" type="default" size="4x"]
[/column]
[column]
[icon icon="freebsd" category="fab" type="default" size="4x"]
[/column]
[column]
[icon icon="suse" category="fab" type="default" size="4x"]
[/column]
[column]
[icon icon="raspberry-pi" category="fab" type="default" size="4x"]
[/column]
[/row]
[space]
[/align]
[/container]

[container layout="small"]
[align type="center]
[space thickness="10"]

## Libre et open-source

[hr width="20"]

[row]
[column]
![GPL v3 logo](images/gpl_v3_logo.png "GPL v3 logo")
[/column]
[column]
![CC logo](images/cc_logo.png "CC logo")
[/column]
[/row]

[space]

Solarus est un logiciel libre; vous pouvez le redistribuer et/ou le modifier sous les termes de la **GNU General Public License** telle que publiée par la Free Software Foundation, soit la version 3 de la licence, ou (selon votre choix) une version supérieure.

Les ressources incluses sont également libres, elles sont sous licence **Creative Commons Attribution-ShareAlike 4.0 international license** (CC BY-SA 4.0).

[space]

[/align]
[/container]

[highlight]
[container layout="small"]
[align type="center]

[space thickness="10"]

## C'est parti !

[hr width="10"]

Prêt(e) à créer votre première quête Solarus ?

[button type="outline-primary" label="Voir le code" url="https://gitlab.com/solarus-games" icon="gitlab" icon-category="fab" icon-size="1x"]
[space orientation="vertical" thickness="40"]
[button type="primary" label="Télécharger" url="/fr/solarus/download" icon="download" icon-category="fa" icon-size="1x"]

[space thickness="70"]

[/align]
[/container]
[/highlight]
