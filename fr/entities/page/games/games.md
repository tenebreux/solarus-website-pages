[container]

# {title}

[row]
[column width="3"]
[/column]
[column width="9"]
[entity-search text-placeholder="Rechercher"]
[/column]
[/row]

[space]

[row]

<!--Filters-->
[column width="3"]
[box-highlight]
[entity-filter entity-type="game" filter="types" title="Genre"]
[entity-filter entity-type="game" filter="languages" title="Langue"]
[entity-filter entity-type="game" filter="states" title="État"]
[/box-highlight]
[/column]

<!--Games -->
[column width="9"]
[game-listing]
[/column]

[/row]

[/container]