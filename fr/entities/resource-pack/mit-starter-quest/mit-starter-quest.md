### Présentation

Ce pack de ressources contient des scripts et des ressouces totalement libres de droits, dans le **domaine public**, contrairement au [Free Resource Pack](/fr/ressource-pack/free-resource-pack) sous licence GPL.

Il vous permet de réaliser un jeu propriétaire et commercial sans devoir rendre votre jeu open-source.
