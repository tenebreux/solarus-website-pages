Avez-vous déjà rêvé de portez des sous-vêtements Solarus ?

Eh bien... c'est désormais possible. Solarus a désormais une ligne de vêtements incluant des T-shirts, des hauts, des sweats à capuches, des casquettes et mêmes des sacs !

![tshirt](images/spreadshirt-shop.jpg)

Tous les bénéfices iront directement à l'association à but non-lucratif [Solarus Labs](/fr/about/nonprofit-organization) que nous avons créée en Janvier 2020. L'argent sera utilisé pour aider Solarus, en payant les coûts d'hébergement ou en achetant du matériel sur lequel vous voudriez voir Solarus être porté, par exemple. Nous envisageons également d'organiser des évènements comme des concours ou des rencontres, si les sommes nous le permettent.

Avouez que vous auriez l'air bien plus cool avec une casquette à l'effigie de Solarus Quest Editor, non ?

Ah. Et *oui*, vous pouvez réellement acheter un boxer Solarus.

[![boxer](images/boxer.jpg)](https://shop.spreadshirt.fr/solarus-labs/solarus+engine+logotype-A5e838b6b22250978b8ab2ec8?productType=146)

Toutes ces merveilles sont disponibles dans la [boutique Spreadshirt](https://shop.spreadshirt.fr/solarus-labs). Amusez-vous bien à déambuler dans les rayons de notre boutique !

[![banner](images/spreadshirt-banner.png)](https://shop.spreadshirt.fr/solarus-labs)
