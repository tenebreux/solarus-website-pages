Chose promise, chose due !

Cela faisait longtemps qu'on en parlait et qu'ils étaient réclamés : les [tutoriels pour apprendre à utiliser Solarus](/fr/development/tutorials) font leur entrée sur le site !

Les tutoriels vont se présenter de la manière suivante :

- Il est possible de créer un livre virtuel, correspondant à un ensemble de chapitres et de pages.
- Actuellement, il y a deux livres : le [tutoriel vidéo](/fr/development/tutorials/solarus-video-tutorial) de Christopho, et le nouveau [tutoriel écrit](/fr/development/tutorials/solarus-official-guide), pas encore aussi fourni que le tutoriel vidéo.
- Tout se base sur des fichiers markdown repertoriés sur le [dépôt Gitlab des pages du site](https://gitlab.com/solarus-games/solarus-website-pages).
- Chacun peut forker, éditer et/ou ajouter des pages, puis créer une pull request pour ajouter ses modifications au website.

Merci à Binbin pour avoir mis en place le système. Nous espérons que les contributions seront nombreuses afin d'étoffer le guide officiel, avec des chapitres sur les bases ou les fonctionnalités plus avancées apportées par Solarus 1.6.
