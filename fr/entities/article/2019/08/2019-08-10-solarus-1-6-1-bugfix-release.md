Nous avons tout juste publié une version de maintenance pour Solarus, qui est désormais en version **1.6.1**. Cette nouvelle version contient principalement des correctifs et ajustements suite au récent passage à OpenGL lors de la version 1.6.0.

Quelques nouvelles fonctionnalités vont également plaire aux créateurs de quêtes, en particulier la possibilité de construire
un package de votre quête automatiquement depuis Solarus Quest Editor.

## Changelog

Voici la liste exhaustive de tous les changements apportés par Solarus 1.6.1, qui représentent près de huit mois de travail.
Merci à std::gregwar, hhromic, ClueDrew et toutes les personnes qui ont contribué à cette version !

### Changements pour Solarus 1.6

![Solarus logo](2019-08-10-solarus-1-6-1-bugfix-release/solarus_logo.png)

#### Changements du moteur

* Ajout d'un argument `-force-software-rendering` pour solarus-run (stdgregwar).
* Ajout d'une option pour afficher ou cacher le curseur de la souris au démarrage (#1263).
* Ajout d'une option CMake pour supporter la compilation avec OpenGL ES (#1270).
* Ajout d'une option CMake pour désactiver l'historique des erreurs dans un fichier (#1276).
* Ajout de compteurs de performance dans le moteur (#1280).
* Ajout d'un argument pour démarrer le jeu en plein écran (#1281).
* Ajout d'une combinaison de boutons de joypad pour quitter Solarus (#1283).
* Ajout d'une option CMake pour contrôler la compilation des tests unitaires (#1288).
* Ajout du chemin racine pour les tests unitaires sous Windows (#1291).
* Ajout de l'information de révision git au démarrage (#1292).
* Ajout d'un argument pour changer la deadzone du joypad (#1293).
* Ajout d'un avertissement si une surface est plus grande que 2048x2048 pixels (#1294).
* Ajout du support d'objets portés avec plusieurs directions (#1392).
* Correction du changement d'ordre des sprites du héros (#1243).
* Correction d'un crash en démarrant un mouvement sur le héros pendant hero:on_state_changed() (#1354).
* Correction d'un crash en changeant l'état du héros pendant qu'il tire un bloc (#1371).
* Correction d'un crash en se noyant après être tombé dans un téléporteur (#1353).
* Correction d'un crash lorsque des escaliers sont désactivés au démarrage de la map avec des tiles dynamiques (#1366).
* Correction d'un crash lorsqu'un état du héros se termine dans une coroutine (#1374).
* Correction de la command d'action déactivée après avoir affiché un dialogue depuis le menu de pause (#1408).
* Correction du héros blessé par les ennemis dans escaliers en spirale (#1038).
* Correction des escaliers ne s'activant pas pendant un état personnalisé avec direction fixe (#1364).
* Correction d'un crash lorsqu'un objet ramassable suit un flux (#1361).
* Correction du mouvement de la caméra bloqué par les séparateurs (#1351).
* Correction des sauteurs ne fonctionnant pas si la taille du héros n'est pas 16x16 (#1381).
* Correction des blocs ne fonctionnant pas si la taille du héros n'est pas 16x16 (#1383).
* Correction des blocs bloqués par les séparateurs (#1356).
* Correction d'un crash dans entity:overlaps(...) avec un mode de collision incorrect (#1041).
* Correction de la vitesse des mouvements droits réinitialisée en changeant la vitesse ou l'angle.
* Correction de sol.menu.start autorisant de démarrer un menu plusieurs fois (#1044).
* Correction du renderer GL pour les appareils GLES 2.0 non-Android (#1267).
* Correction de problèmes de compatibilité OpenGL.
* Correction d'une erreur de FindOpengl à propos du choix entre GLVND et GLX (#1320).
* Correction de conversions incorrectes entre les coordonnées de fenêtre et de quêtes (#1340).
* Correction des coordonnées de souris et de toucher utilisant une taille de fenêtre incorrecte. (#1268).
* Correction du passage de plein écran vers fenêtré (#1269).
* Correction de la surface d'écran non initialisée en mode 640x480 (#1273).
* Correction de l'initialisation de la surface d'écran sans fenêtre (#1274).
* Correction de la sortie du mode plein écran sous Windows (#1284).
* Correction d'un avertissement lorsque la taille de quête par défaut n'est pas supportée (#1376). 
* Correction de la compilation avec mingw-w64 sous Linux (#1282).
* Correction de la détection de la version de LuaJIT dans CMake (#1277).
* Correction de la compilation de la GUI avec mingw-w64 sous Linux (#1285).
* Correction du lancement des tests unitaires depuis le répertoire de compilation (#1289).
* Correction de tests unitaires échouant sous Windows (#1290).

#### Changements de l'API Lua

Cette version ajoute de nouvelles fonctionnalités sans introduire
d'incompatibilités.

* Ajout de méthodes game:get/set_transition_style() (#1368).
* Ajout de méthodes state:get/set_can_use_teletransporter/switch/stream() (#1363).
* Les types surface, text_surface et timer sont désormais indexables (#1394).

### Changements pour Solarus Quest Editor 1.6.0

![Solarus Quest Editor logo](2019-08-10-solarus-1-6-1-bugfix-release/sqe_logo.png)

* Ajout de la possibilité de construire un package de la quête (#431).
* Éditeur de maps : allow teletransporters to have any size multiple of 8 pixels.
* Éditeur de maps : amélioration des performances de la suppression d'entités multiples.
* Éditeur de maps : amélioration des performances du changement de couche d'entités multiples (#454).
* Éditeur de maps : correction de sélection incorrect après l'annulation d'un changement de couches.
* Éditeur de tilesets : amélioration des performances de la suppression de multiples motifs (#456).
* Éditeur de sprites : correction d'un message d'erreur persistant à propos de l'image source manquante (#451).
* Éditeur de dialogues : correction des sauts de ligne.
* Quête initiale : suppression d'un fichier propriétaire ajouté par erreur.
* Ajout d'une option "Forcer le rendu software" au lancement de la quête (stdgregwar).

### Changements pour Zelda Mystery of Solarus DX 1.12.1

![Mystery of Solarus DX logo](2019-08-10-solarus-1-6-1-bugfix-release/mos_dx_logo.png)

* Correction d'une faute dans les textes en anglais (#128).
* Correction de fautes dans les textes en allemant (#128).
* Correction d'une fonction obsolète depuis Solarus 1.6 (#131).

### Changements pour Zelda Mystery of Solarus XD 1.12.1

![Mystery of Solarus XS logo](2019-08-10-solarus-1-6-1-bugfix-release/mos_xd_logo.png)

* Correction d'une torche affichée au-dessus du héros dans le donjon 1 (#55).

### Changements pour Zelda Return of the Hylian SE 1.2.1

![Return of the Hylian SE logo](2019-08-10-solarus-1-6-1-bugfix-release/roth_se_logo.png)

* Correction d'une erreur d'affichage dans un sprite de personnage (#114).

### Changements pour Zelda XD2 Mercuris Chess 1.1.1

![XD2 Mercuris Chess logo](2019-08-10-solarus-1-6-1-bugfix-release/zsxd2_logo.png)

* Correction du format de Solarus 1.6.
* Correction d'un problème où le joueur pouvait rester bloqué dans une pierre près de l'Île des Rubis (#141).
* Correction d'une erreur d'affichage dans un sprite de personnage (#142).
