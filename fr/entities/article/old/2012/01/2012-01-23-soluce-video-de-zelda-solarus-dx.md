Vous êtes bloqué dans [url=http://www.zelda-solarus.com/jeu-zsdx-download]Zelda Mystery of Solarus DX[/url] ? Il vous manque un Fragment de C?ur ?

Nous venons de commencer une série de vidéos qui vous guideront tout au long du jeu. Dans ces vidéos, commentées par nos soins, nous traverserons tout le jeu afin de le finir à 100 % et en expliquant chaque passage.

Bien sûr, comme toujours, utilisez cette solution avec parcimonie pour ne pas gâcher le jeu. Cherchez d'abord par vous-mêmes, parlez aux villageois et lisez bien les précieux indices... Consultez la solution seulement si vous êtes vraiment bloqués. ^_^

[list]
[li][url=http://www.zelda-solarus.com/jeu-zsdx-soluce]Solution de Zelda Mystery of Solarus DX[/url][/li]
[/list]

Pour l'instant, la solution va jusqu'à la fin du premier donjon. Deux épisodes sont ainsi disponibles, ils sont commentés par Binbin et moi. Les épisodes suivants arriveront progressivement dans les prochaines semaines. Par la suite, vous aurez droit à mes commentaires avec différents binômes. Merci à Binbin pour le générique. :)

PS : en ce qui concerne les spoilers, dans chaque vidéo, on ne révèle rien sur la suite du jeu. Vous pouvez donc regarder sans risque les vidéos des passages que vous avez déjà fait.