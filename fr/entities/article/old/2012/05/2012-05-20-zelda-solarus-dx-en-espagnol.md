Une nouvelle version de [url=http://www.zelda-solarus.com/jeu-zsdx]Zelda Mystery of Solarus DX[/url] vient de sortir. Il s'agit de la version 1.5.1.

Au programme, de nombreuses corrections et améliorations mineures, et surtout une première version bêta de la traduction en espagnol. (Notez que la traduction en anglais est disponible depuis la version 1.5.0.)

Liste des changements :
[list]
[li]Traduction espagnole disponible (bêta)[/li]
[li]Corrections diverses dans la traduction anglaise[/li]
[li]Tour des Cieux : le héros pouvait se retrouver dans un mur en tombant[/li]
[li]Tour des Cieux : améliorations mineures[/li]
[li]Correction d'un bug avec les flammes bleues lancées par certains boss et qui restaient bloquées[/li]
[li]Ancien Château : améliorations mineures[/li]
[li]Dédale d'Inferno : il était possible de contourner une énigme du donjon[/li]
[/list]

Comme d'habitude, vous pouvez installer directement la nouvelle version à la place de l'ancienne et vos sauvegardes seront conservées.

[list]
[li][url=http://www.zelda-solarus.com/jeu-zsdx-download]Télécharger Zelda Mystery of Solarus DX[/url][/li]
[/list]

Merci à Clow_eriol, Emujioda, LuisCa, Musty, Xadou, Guopich et falvarez pour la traduction du jeu en espagnol. C'est un magnifique travail d'équipe. ^_^
[b]Attention :[/b] la version espagnole est en bêta-test, c'est-à-dire qu'elle nécessite d'être relue et vérifiée. Toute aide est la bienvenue pour cela : n'hésitez pas à [url=http://www.zelda-solarus.com/contact.php]me contacter[/url] si vous parlez espagnol.