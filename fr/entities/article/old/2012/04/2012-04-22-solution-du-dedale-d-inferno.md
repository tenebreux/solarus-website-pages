Ça continue !
Voici deux nouveaux épisodes de la [url=http://www.zelda-solarus.com/jeu-zsdx-soluce]soluce vidéo de Zelda Mystery of Solarus DX[/url].
Cette fois-ci, c'est avec Neovyse que j'ai eu le plaisir de réaliser les commentaires. :)

[list]
[li] [url=http://www.youtube.com/watch?v=jbUnejEjXrs]Épisode 11 : du niveau 5 au niveau 6[/url][/li]
[li] [url=http://www.youtube.com/watch?v=NoAIuV9d4qA]Épisode 12 : niveau 6 - Dédale d'Inferno[/url][/li]
[/list]

On commence donc à arriver dans les donjons vraiment difficile. J'espère que ça vous aidera, mais comme toujours, n'abusez pas des solutions, consultez-les seulement si vous êtes vraiment bloqués ! ^_^