Alors que la programmation vient de redémarrer en trombe après une pause dûe aux vacances, voici que Neovyze vous propose le tout premier artwork de Zelda: Mystery of Solarus DX ! Bien entendu, il s'agit d'un artwork de Link (cliquez pour agrandir) :

[center][url=http://www.zelda-solarus.com/images/zsdx/artworks/link_medium.png][img]http://www.zelda-solarus.com/images/zsdx/artworks/link_small.png[/img][/url][/center]

On espère qu'il vous plaira ^_^.

Du côté de la programmation, comme je le disais il y a eu une pause de deux semaines mais ça a repris en même temps que la reprise du travail. Actuellement, je suis en train de faire en sorte que Link puisse sauter depuis le haut d'une falaise. A bientôt pour d'autres nouvelles :).