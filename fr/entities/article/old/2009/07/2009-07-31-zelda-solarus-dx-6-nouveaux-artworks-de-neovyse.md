Bonjour à tous,

Aujourd'hui nous vous dévoilons 6 nouveaux artworks de [url=http://www.zelda-solarus.com/jeu-zsdx]Zelda : Mystery of Solarus DX[/url]. Réalisés par Neovyse, ils représentent les personnages principaux du jeu ainsi que l'Amulette du Solarus, un objet très important pour l'histoire :).
Cliquez sur un artwork pour l'agrandir.

[center][url=http://www.zelda-solarus.com/images/zsdx/artworks/zelda_big.jpg][img]http://www.zelda-solarus.com/images/zsdx/artworks/zelda_small.jpg[/img][/url]
Zelda

[url=http://www.zelda-solarus.com/images/zsdx/artworks/sahasrahla_big.jpg][img]http://www.zelda-solarus.com/images/zsdx/artworks/sahasrahla_small.jpg[/img][/url]
Sahasrahla

[url=http://www.zelda-solarus.com/images/zsdx/artworks/tom_big.jpg][img]http://www.zelda-solarus.com/images/zsdx/artworks/tom_small.jpg[/img][/url]
Tom

[url=http://www.zelda-solarus.com/images/zsdx/artworks/billy_big.jpg][img]http://www.zelda-solarus.com/images/zsdx/artworks/billy_small.jpg[/img][/url]
Billy le Téméraire

[url=http://www.zelda-solarus.com/images/zsdx/artworks/agahnim_big.jpg][img]http://www.zelda-solarus.com/images/zsdx/artworks/agahnim_small.jpg[/img][/url]
Agahnim

[url=http://www.zelda-solarus.com/images/zsdx/artworks/amulette_big.jpg][img]http://www.zelda-solarus.com/images/zsdx/artworks/amulette_small.jpg[/img][/url]
L'Amulette du Solarus[/center]

Link a également été retravaillé :
[center][url=http://www.zelda-solarus.com/images/zsdx/artworks/link_big.jpg][img]http://www.zelda-solarus.com/images/zsdx/artworks/link_small.jpg[/img][/url]
Link[/center]

Félicitons Neovyse pour ce travail :P