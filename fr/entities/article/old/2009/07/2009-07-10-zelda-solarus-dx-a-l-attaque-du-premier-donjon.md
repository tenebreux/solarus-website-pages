Bonsoir à tous,

Je n'avais pas donné de nouvelles du développement depuis un certain temps. Il faut dire que l'actualité Zelda a été fournie ces derniers temps avec Zelda Wii, Spirit Tracks, l'E3 et la sortie du premier manga Zelda en français. Et puis publier la première [url=http://www.youtube.com/watch?v=r3e7t0QvImY]vidéo de Zelda Mystery of Solarus DX[/url] était également un petit événement ^^.

La création du début du jeu a bien avancé. Toutes les maps sont désormais faites jusqu'à l'entrée du premier donjon. En particulier, la première vraie grotte du jeu, celle où rencontre Tom qui nous prête son Boomerang, est terminée. Tom a été dessiné par Newlink, dessinateur des nombreux personnages aux graphismes inédits dans le jeu.

[center]
[img]http://www.zelda-solarus.com/images/zsdx/tom.png[/img]
[/center]
Et la grotte de Tom a été en partie refaite pour l'occasion:
[center]
[img]http://www.zelda-solarus.com/images/zsdx/tom_cave_1.png[/img]
[img]http://www.zelda-solarus.com/images/zsdx/tom_cave_2.png[/img]
[/center]

Le lancer du boomerang est plus précis et on peut mieux le contrôler que dans Zelda Solarus 1, ce qui donne lieu à des énigmes inédites dont vous pouvez voir un aperçu ci-dessus. Les autres objets de l'inventaire disponibles au début du jeu (Croissant, Flacon Magique...) sont eux aussi terminés.
En résumé, tout est fait ou presque jusqu'à l'entrée du premier donjon. Je travaille actuellement à créer des types d'ennemis, notamment ceux qui sont capables de poursuivre Link malgré les obstacles : ces ennemis vont être un peu moins bêtes que les autres car ils seront capables de rechercher un chemin en évitant les obstacles ;)
Après cela, je passerai à la création du premier donjon. La sortie de la démo approche donc... Mais ne soyez pas trop pressés car il reste tout de même des gros morceaux comme le boss du donjon mais aussi l'intro du jeu... Et même une fois le donjon et l'intro finis, il faudra tester le jeu comme il se doit afin de sortir une démo de qualité. La bonne nouvelle, c'est que je suis en vacances et que je compte bien en profiter :)

Sur ce, je vous laisse sur d'autres images du jeu ^_^.

[center]
[img]http://www.zelda-solarus.com/images/zsdx/icy_cave.png[/img] [img]http://www.zelda-solarus.com/images/zsdx/shop.png[/img]
[img]http://www.zelda-solarus.com/images/zsdx/water_in_bottle.png[/img] [img]http://www.zelda-solarus.com/images/zsdx/smith_cave.png[/img]
[/center]

Vous pouvez retrouver l'ensemble des captures d'écran dans la [url=http://www.zelda-solarus.com/jeu-zsdx-images]galerie d'images[/url].
