Bonsoir à tous, le moment est venu ! ^_^
Ca y est, nous sommes le vendredi 18 décembre, alors sans plus attendre... Je suis heureux de vous annoncer que vous pouvez y jouer... maintenant !

[list]
[li][url=http://www.zelda-solarus.com/download-zsdxdemo_fr_win32]Télécharger la démo (version française, Windows)[/url][/li]
[li][url=http://www.zelda-solarus.com/jeu-zsdx-demo]Autres versions[/url][/li]
[/list]

J'espère que la démo vous plaira, sachez en tout cas que j'ai essayé de ne pas dévoiler absolument tout à l'avance :P. Amusez-vous bien !