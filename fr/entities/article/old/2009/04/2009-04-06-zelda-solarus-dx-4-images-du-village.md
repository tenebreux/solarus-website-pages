Bonsoir à tous,

En ce début de printemps, j'ai des nouvelles à vous donner sur le développement du projet. Pas d'annonce fracassante le 1er avril, comme vous avez pu le voir, mais on ne va pas non plus vous faire le coup tous les ans :P. Et puis de toute façon, il n'y avait aucune démo de prête (fictive ou non !) ni aucune date raisonnable à annoncer : le coup de la fausse date, on l'a déjà fait en 2003, et le coup de la vraie date, on l'a déjà fait en 2005 ! Pour tout vous dire, on avait bien quelques idées liées à Zelda Solarus DX pour ce poisson d'avril, mais cela aurait demandé trop de travail... du temps que j'ai préféré consacrer au développement du jeu pour de vrai. Bref, au lieu de cela, on a préféré placer quelques bannières de pub décalées et lancer des [url=http://www.zelda-solarus.com/zsphilo.php]débats philosophiques[/url] sur Zelda, des débats qui vous ont beaucoup inspirés [url=http://forums.zelda-solarus.com/index.php/board,60.0.html]sur le forum[/url] ! Link est-il l'incarnation d'un être idéal ? Le système économique d'Hyrule peut-il être considéré comme un modèle pour nos sociétés ? Des questions passionnantes qui ont révélé la plume de certains, et qui continuent encore à vous inspirer presque une semaine après le 1er avril :D.

Mais revenons-en au sujet de cette mise à jour. Le jeu avance de mieux en mieux. Une grande partie du village du début du jeu est maintenant faite. A la demande de beaucoup, le chien qui vous bloquait au début du jeu a été remplacé par un autre animal plus Zelda-esque ;). La maison des rubis est terminée, ainsi que deux autres maisons : celle de Sahasrahla, l'Ancien qui vous aidera beaucoup dans votre aventure, mais aussi une maison inédite. En effet, le village sera plus vivant que dans le premier ZS, il y aura plus de personnages (dont beaucoup sont dessinés par Newlink) et ils vous donneront plus d'indications pour débuter, mais aussi tout au long de la quête.

Parallèlement à cela, j'ai ajouté ou amélioré quelques éléments du moteur qui n'étaient pas encore au point. Le plus intéressant de ces éléments est sans aucun doute le système de scrolling entre les maps de la carte du monde ou des donjons. Quand vous arrivez au bord d'un écran, la transition vers l'écran suivant se fait avec du scrolling, comme dans les vrais Zelda 2D et quelques rares Zelda amateurs :). Même Zelda Mercuris' Chest ne faisait pas ça ^^.

Je tiens à signaler enfin la participation de Yoshi04 qui a déniché plusieurs bugs particulièrement subtils... Grâce à son aide et à celle des futurs testeurs, on peut espérer que le projet tiendra la route, en tout cas c'est bien parti :super:.

Voilà, il ne me reste plus qu'à vous montrer quelques images inédites du village. Deux d'entre elles sont affichées ci-dessous, deux autres sont disponibles avec le reste des captures d'écran dans la galerie d'images.

[center][img]http://www.zelda-solarus.com/images/zsdx/grandma.png[/img][/center]
[center][img]http://www.zelda-solarus.com/images/zsdx/village_boy.png[/img][/center]

[list]
[li][url=http://www.zelda-solarus.com/jeu-zsdx-images]Galerie d'images[/url][/li]
[/list]