<a href="../data/fr/entities/article/old/2013/10/images/solarus-logo-black-on-transparent.png"><img class="aligncenter" alt="Logo du moteur Solarus" src="/images/solarus-logo-black-on-transparent-300x90.png" width="300" height="90" /></a>

Une nouvelle version du moteur Solarus vient de sortir, de même qu'une mise à jour de Zelda Mystery of Solarus DX et de Zelda Mystery of Solarus XD.

Si vous suivez notre <a href="http://www.youtube.com/watch?v=9n-aottUQUA&amp;list=PLzJ4jb-Y0ufySXw9_E-hJzmzSh-PYCyG2">tutoriel vidéo</a> ou <a href="http://wiki.solarus-games.org/doku.php?id=fr:tutorial:create_your_2d_game_with_solarus">écrit</a>, vous savez déjà que de nombreuses améliorations étaient promises pour Solarus 1.1. Cette nouvelle version est désormais disponible en téléchargement !

Il y a énormément de nouveautés dans le moteur, qui vous intéresseront si vous l'utilisez pour développer votre propre projet de jeu. Parmi les plus importantes, un nouveau type d'entité est disponible : les séparateurs, qui vous permettent de séparer visuellement une map en plusieurs régions. Deux nouvelles sortes de terrain sont proposées : la glace et les murets. Le format de certains fichiers de données a changé, en particulier celui des sprites qui est maintenant beaucoup plus lisible. Autre point essentiel : chaque jeu peut maintenant personnaliser sa boîte de dialoge et son menu de game-over. Les ennemis et les blocs peuvent maintenant tomber dans les trous et la lave. Solarus 1.1 apporte aussi son lot de corrections de bugs.
<ul>
	<li>Télécharger <a href="http://www.solarus-games.org/downloads/solarus/win32/solarus-1.1.0-win32.zip">Solarus 1.1 + l'éditeur de quêtes</a> pour Windows</li>
	<li>Télécharger le <a href="http://www.solarus-games.org/downloads/solarus/solarus-1.1.0-src.tar.gz">code source</a></li>
	<li>Liste complète des <a href="http://www.solarus-games.org/2013/10/13/solarus-1-1-released/">changements</a></li>
	<li><a href="http://www.solarus-games.org/">Blog de développement Solarus</a></li>
	<li>Comment <a href="http://wiki.solarus-games.org/doku.php?id=fr:migration_guide">convertir votre quête</a> de Solarus 1.0 vers Solarus 1.1</li>
</ul>
Du côté de nos créations, elles ont été mises à jour pour profiter de ces améliorations et corrections de bugs. En particulier, vous pouvez maintenant enfin pousser les ennemis dans les trous et la lave :)
<ul>
	<li>Télécharger <a href="http://www.zelda-solarus.com/zs/article/zmosdx-telechargements/">Zelda Mystery of Solarus DX 1.7</a></li>
	<li>Télécharger <a href="http://www.zelda-solarus.com/zs/article/zmosxd-telechargements/">Zelda Mystery of Solarus XD 1.7</a></li>
</ul>
Ce n'est pas tout puisque Zelda Mystery of Solarus DX 1.7 est maintenant traduit en chinois simplifié et chinois traditionnel (merci Sundae et Rypervenche !). Zelda Mystery of Solarus XD est quant à lui en cours de traduction en espagnol (merci Xexio !). Une autre amélioration visible est que le logo Solarus qui apparaît au démarrage de nos jeux, réalisé par Neovyse, est maintenant animé grâce au travail de Maxs (merci !).

Autre nouveauté très très attendue : cette version sera d'ici quelques heures officiellement disponible sur Android grâce au portage réalisé par Sam101 (merci aussi !). Enfin, sachez qu'elle fonctionne également sur les consoles open-source <a href="http://openpandora.org/">OpenPandora</a> et <a href="http://www.gcw-zero.com/">GCW-Zero</a>.

N'hésitez pas à nous signaler tout problème si par hasard un bug nous aurait échappé ! En tout cas, j'espère que vous apprécierez ces améliorations. Pendant ce temps, de notre côté, nous continuons le travail sur Mercuris' Chest et sur Solarus 1.2.