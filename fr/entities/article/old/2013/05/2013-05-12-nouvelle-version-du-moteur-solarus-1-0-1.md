<a href="../data/fr/entities/article/old/2013/05/images/solarus-logo-black-on-transparent.png"><img class="aligncenter size-medium wp-image-36532" alt="Logo du moteur Solarus" src="/images/solarus-logo-black-on-transparent-300x90.png" width="300" height="90" /></a>

Une mise à jour de notre moteur de jeu Solarus vient de sortir !

Au programme, une vingtaine de corrections de bugs et des améliorations dans l'éditeur de quêtes. Les détails sont disponibles sur le <a href="http://www.solarus-games.org/2013/05/12/solarus-1-0-1-released/">blog de développement</a>. La principale amélioration de l'éditeur de maps est que vous pouvez maintenant afficher ou non chaque type d'entité (ennemis, coffres, tiles). Je ne peux que vous conseiller de passer à cette nouvelle version si vous utilisez Solarus pour développer un jeu :).
<ul>
	<li><span style="line-height: 13px;">Télécharger <a href="http://www.solarus-games.org/downloads/solarus/win32/solarus-1.0.1-win32.zip">Solarus 1.0.1 + l'éditeur de quêtes</a> pour Windows</span></li>
	<li>Télécharger <a href="http://www.solarus-games.org/downloads/solarus/macosx/solarus-1.0.1-macosx64.zip">Solarus 1.0.1 + l'éditeur de quêtes</a> pour Mac OS X</li>
	<li>Télécharger le <a href="http://www.solarus-games.org/downloads/solarus/solarus-1.0.1-src.tar.gz">code source</a></li>
</ul>
PS : la version Mac OS X est maintenant disponible ! Merci à Vlag :)