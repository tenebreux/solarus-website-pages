Lorsque les ténèbres apparaissent, la lame d'un héros émerge toujours.
[url=http://www.zelda-solarus.com/revelation-marees]Tel le reflux des marées, l'histoire se répète inlassablement.[/url]

Le héros vertueux révèlera le mystère de la prophétie des sept années.
[url=http://www.zelda-solarus.com/revelation-sagesse]Ce n'est qu'avec sagesse que le héros découvrira sa destinée.[/url]

Depuis les ténèbres peut jaillir la perfection.
[url=http://www.zelda-solarus.com/revelation-verite]La vérité n'est jamais cachée.[/url]

Sept années se sont écoulées depuis l'époque d'origine.
[url=http://www.zelda-solarus.com/revelation-patience]La patience est la vertu du vrai héros.[/url]

