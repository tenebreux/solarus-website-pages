Bonjour à tous,

Je vous propose aujourd'hui une nouvelle vidéo de Zelda Mystery of Solarus DX. Il s'agit d'un trailer qui montre diverses séquences du jeu, dont quelques éléments inédits comme le deuxième donjon, l'arc, les escaliers?
Il faut noter que la musique de ce trailer est elle aussi inédite, elle a été créée par notre compositeur George. :)

Enjoy ! ^_^

[list]
[li][url=http://www.youtube.com/watch?v=HUMcFAdBJ_g]Voir le trailer[/url][/li]
[li][url=http://www.zelda-solarus.com/jeu-zsdx-videos]Toutes les vidéos[/url][/li]
[/list]