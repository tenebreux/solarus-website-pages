Lorsque j'ai publié, il y a 6 semaines, le [url=http://www.youtube.com/watch?v=BUxREyXILLs]trailer[/url] annonçant la date du 16 décembre, je dois vous avouer que rien ne permettait de savoir si le jeu serait fini à cette date :ninja:.

D'ailleurs, je n'avais même pas demandé son avis au reste de [url=http://zelda-solarus.com/jeu-zsdx-equipe]l'équipe[/url], en particulier Metallizer, Binbin et Newlink alors qu'il leur restait beaucoup de travail à faire. Je ne les avais même pas prévenus :P

Le but était de me mettre la pression pour accélérer le développement. En gardant en tête que si les délais étaient trop courts, rien n'empêchait de repousser la date de sortie ou de sortir une version bêta le 16 décembre au lieu d'une version stable.

Mais c'était sans compter sur la motivation d'une équipe qui a créé Zelda Mystery of Solarus XD de A à Z en 7 petites semaines.

Et après 350 commits (c'est-à-dire ajouts ou modifications d'éléments du moteur et du jeu), des nuits très courtes (une nuit blanche en allant bosser après :mrgreen:) et beaucoup de café, on voit la fin ! Maintenant, on peut vous dire avec beaucoup plus de certitude que le jeu sortira bel et bien le vendredi 16 décembre, très bientôt donc. Bravo à Metallizer, Binbin, Newlink et Mymy qui ont fourni un travail assez colossal (et de qualité !) pour y arriver. Et merci aux testeurs (Thyb, Morwenn, BenObiWan et tous les autres) qui, malgré les délais serrés qu'on leur impose, vérifient tout le jeu de fond en comble.

Le 16 décembre, vous pourrez jouer à Zelda Mystery of Solarus DX, et moi je pourrai attaquer Zelda Skyward Sword ^_^