Après quelques semaines d'interruption liées au développement de notre [url=http://www.zelda-solarus.com/jeu-zsxd]mini-jeu parodique[/url], la création des maps de Zelda : Mystery of Solarus DX est repartie de plus belle. Plusieurs donjons sont en cours de développement par Metallizer et moi.

Pour être précis, voici une estimation approximative de l'avancement de chaque donjon :
[list]
[li]Niveau 1 (Donjon de la Forêt) : 100 %[/li]
[li]Niveau 2 (Caverne de Roc) : 98 %[/li]
[li]Niveau 3 (Antre de Maître Arbror) : 95 %[/li]
[li]Niveau 4 (Palais de Beaumont) : 0 %[/li]
[li]Niveau 5 (Ancien Château d'Hyrule) : 0 %[/li]
[li]Niveau 6 (Dédale d'Inferno) : 40 %[/li]
[li]Niveau 7 (Temple du Cristal) : 0 %[/li]
[li]Niveau 8 (Donjon des Pics Rocheux) : 80 %[/li]
[li]Niveau 9 (Temple des Souvenirs) : 1 %[/li]
[/list]
Comme vous le voyez, on ne les fait pas dans l'ordre, on les fait plutôt selon l'envie du moment. Ce qui est intéressant, c'est que les donjons vont de plus en plus vite à se créer car le moteur de jeu devient très complet, de même que l'éditeur de maps qui nous permet d'être efficaces.

Nous sommes donc en mesure de publier 18 nouvelles captures d'écrans de quatre donjons différents. En voici 5 ci-dessous, les autres sont dans la [url=http://www.zelda-solarus.com/jeu-zsdx-images]galerie d'images[/url]. Les spécialistes reconnaîtront.

[center]
[img]http://www.zelda-solarus.com/images/zsdx/dungeon_8_prickles.png[/img]
[img]http://www.zelda-solarus.com/images/zsdx/dungeon_6_crystals.png[/img]
[img]http://www.zelda-solarus.com/images/zsdx/dungeon_3_enemies.png[/img]
[img]http://www.zelda-solarus.com/images/zsdx/dungeon_2_miniboss.png[/img]
[img]http://www.zelda-solarus.com/images/zsdx/dungeon_2_big_chest.png[/img]
[/center]

[list]
[li][url=http://www.zelda-solarus.com/jeu-zsdx-images]Voir toutes les images[/url][/li]
[/list]