La capture d'écran de la semaine de Zelda Mercuris' Chest vous emmène dans des montagnes enneigées mais où la lave n'est pourtant jamais bien loin.

<a href="../data/fr/entities/article/old/2017/02/images/volcanoscreen.png"><img class="aligncenter size-medium wp-image-38093" src="/images/volcanoscreen-300x226.png" alt="volcanoscreen" width="300" height="226" /></a>

Bonne semaine à toutes et à tous.

&nbsp;