Le temple que vous voyez sur cette capture d'écran de Zelda Mercuris' Chest est bien caché. Link va devoir ruser pour traverser la forêt qui permet de l'atteindre.

<a href="../data/fr/entities/article/old/2017/02/images/nature.png"><img class="aligncenter size-medium wp-image-38103" src="/images/nature-300x225.png" alt="nature" width="300" height="225" /></a>

Certains d'entre vous pourraient reconnaître cet endroit car je l'avais réalisé au cours d'un <a href="https://www.youtube.com/watch?v=AWgEuNkE5Xs&amp;index=3&amp;list=PLzJ4jb-Y0ufxKu9TItbmyvjlJ3eSdsW0a">live-streaming</a>. C'était en 2015 mais on n'avait pas encore publié beaucoup de captures d'écrans !

&nbsp;

&nbsp;