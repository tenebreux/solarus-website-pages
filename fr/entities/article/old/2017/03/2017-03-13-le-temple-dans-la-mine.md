Ceux qui ont joué à la démo de Zelda Mercuris' Chest reconnaîtront sans doute les lieux !

<a href="../data/fr/entities/article/old/2017/03/images/rail_temple_chest.png"><img class="aligncenter size-medium wp-image-38112" src="/images/rail_temple_chest-300x225.png" alt="rail_temple_chest" width="300" height="225" /></a>

Rappelons que le Temple du Rail, donjon qui était dans la démo, sera bien intégré dans le jeu complet mais remasterisé avec des décors inédits. Et quelques petites modifications car 4 trésors dans un seul donjon ça fait beaucoup !