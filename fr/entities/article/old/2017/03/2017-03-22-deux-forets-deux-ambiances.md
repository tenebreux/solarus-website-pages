La capture d'écran de la semaine concernant notre projet Mercuris' Chest vous dévoile aujourd'hui un peu plus la forêt du nord :

<a href="../data/fr/entities/article/old/2017/03/images/torin_temple.png"><img class="aligncenter size-medium wp-image-38117" src="/images/torin_temple-300x225.png" alt="torin_temple" width="300" height="225" /></a>

Complètement différente de celle dont je vous parlais il y a trois semaines, cette forêt est peuplée de créatures fantômatiques qu'il faudra délivrer d'une malédiction.

Vous l'aurez compris, le projet avance avance beaucoup ces temps-ci !