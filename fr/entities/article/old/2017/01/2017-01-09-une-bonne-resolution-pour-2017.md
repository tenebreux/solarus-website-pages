Avant toute chose, toute l'équipe vous souhaite une bonne et heureuse année 2017 !

2016 a été pour la communauté de création de jeux Solarus une année très importante avec la sortie de Solarus 1.5. Mais en 2017, on vous promet moins d'avancées dans Solarus pour vraiment mettre l'accélérateur sur nos jeux en cours de développement.

Avec bien sûr le principal d'entre eux, Zelda Mercuris' Chest, dont voici trois nouvelles captures d'écran. Merci à Newlink pour son aide précieuse !

<a href="../data/fr/entities/article/old/2017/01/images/chateau.png"><img class="aligncenter size-medium wp-image-38066" src="/images/chateau-300x224.png" alt="chateau" width="300" height="224" /></a>

<a href="/images/desert_1.png"><img class="aligncenter size-full wp-image-38067" src="/images/desert_1.png" alt="desert_1" width="295" height="209" /></a>

<a href="../data/fr/entities/article/old/2017/01/images/lac.png"><img class="aligncenter size-medium wp-image-38068" src="/images/lac-300x225.png" alt="lac" width="300" height="225" /></a>

&nbsp;

Et puis comme bonne résolution pour cette nouvelle année, vous montrer une nouvelle capture d'écran chaque semaine ! On s'y engage !