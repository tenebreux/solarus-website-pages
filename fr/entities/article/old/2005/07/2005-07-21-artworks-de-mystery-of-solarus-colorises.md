<p>Forza a eu la superbe idée de coloriser les artworks officiels de Mystery of Solarus !</p>

<p>Leur qualité est ainsi nettement supérieure, et le plaisir de les regarder plus grand ! Pour fêter cela comme il se doit, je vous présente une nouvelle version de Link, plus travaillée, plus manga et plus expressive ! Voici donc ce nouveau Link :</p>

<p align="center"><a href="../data/fr/entities/article/old/2005/07/images/link2.jpg"><img src="/images/link2_mini.jpg" width="117" height="216" border="0"></a></p>

<p>Les autres artworks colorisés illustrent le <a href="http://www.zelda-solarus.com/jeux.php?jeu=zs&zone=notice">mode d'emploi</a> de Zelda : Mystery of Solarus.</p>
