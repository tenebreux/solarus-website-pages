<p><img src="/images/artzmc_01.jpg" width="192" height="280" align="right"></p>

<p>Nous gardions secret depuis de longs mois le titre de notre prochaine création... Et aujourd'hui nous vous le dévoilons  officiellement : "Zelda Mercuris' Chest" !</p>

<p>En Français, pour ceux qui n'auraient pas compris, ça donne "le coffre de Mercuris". Mercuris est un des nombreux personnages  que vous aurez l'occasion de rencontrer au cours de votre aventure. Son rôle dans le scénario a évidemment une importance toute  particulière, à tel point que son nom apparaît dans le titre du jeu ! Mercuris est une créature mi-homme mi-oiseau, mais en fait,  vous le connaissez déjà, puisque c'est lui qui est représenté sur le dessin ci-contre !</p>

<p>L'équipement de Link a été repensé spécialement pour cette aventure. Il y aura des objets inédits et même de nouvelles armes  aux effets surprenants. L'Epée de Link aura des capacités spéciales comme brûler les ennemis à petit feu, les geler sur place ou  encore les désintégrer purement et simplement !</p>

<p>Mais il est temps que je vous en dise un peu plus sur le scénario. L'histoire ne se déroulera pas à Hyrule et pour une fois,  Ganondorf ne sera pas présent ! Link sera plongé dans un monde aussi vaste qu'inconnu et l'ambiance générale du jeu très sombre,  plus que dans la plupart des Zelda. Link aura quelques amis mais surtout beaucoup d'ennemis...</p>

<p>Comme le titre le laisse entendre, il sera question d'un coffre, un précieux coffre que Link va devoir récupérer. Mais notre  ami Mercuris, le gardien du coffre, ne l'entend pas de cette oreille, et il va donner du fil à retordre à notre héros ! En bref, une intrigue mystérieuse et riche en rebondissements : voilà ce qui vous attend !</p>

<p>Mais passons à la deuxième annonce de cette news, une annonce qui concerne la réédition de la démo.</p>

<p align="center"><b>Mardi 26 Avril 2005</b></p>

<p>C'est une date fixée et je pense qu'elle sera respectée...</p>

<p>Vous avez bien lu, le Mardi 26 Avril 2005 ! C'est la date de sortie de la nouvelle version de la démo. Rappelons que cette démo  est une réédition de la première mais avec le moteur du jeu complet. Le développement avance très vite et se déroule sans le  moindre problème. Dans 25 petits jours donc, vous pourrez déjà profiter des nouvelles fonctionnalités de cette démo. Non content  de pouvoir enfin jouer sans subir de plantages intempestifs, vous aurez également la joie et l'excitation de pouvoir choisir entre  le mode plein écran et le mode fenêtré, mais aussi la satisfaction de configurer les touches de votre clavier ou de votre manette,  ou encore de naviguer entre les différents sous-écrans du menu du jeu complet.</p>

<p align="center"><img src="/images/002.png" width="320" height="240"></p>

<p>Et tout ça dans seulement... 600 heures !</p>