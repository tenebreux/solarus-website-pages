<p>Bruno, déjà auteur de plusieurs <a href="http://www.zelda-solarus.com/fans.php">fan arts</a>, a réalisé rien que pour vous une affiche de notre création Zelda : Mystery of Solarus (cliquez pour agrandir) :</p>

<p align="center"><a href="../data/fr/entities/article/old/2005/03/images/affiche.jpg" target="_blank"><img border="0" src="/images/affiche_mini.jpg" width="189" height="242"></a></p>

<p>On espère qu'elle vous plaît, quoi qu'il en soit laissez vos commentaires !</p>

<p>Bruno est graphiste et peut prendre des contrats pour réaliser vos projets. Si vous souhaitez le contacter, <a href="http://www.zelda-solarus.com/mailto:brunodeconinck@hotmail.com">cliquez ici</a>.</p>