Ça bouge beaucoup du côté du moteur Solarus et de Solarus Quest Editor ! Les tutos vidéo de niveau basique sont terminés et publiés, et Solarus 1.5.1 vient de sortir. Rappelons d'abord que la version 1.5, disponible depuis cet été, vous propose comme grande nouveauté un lanceur de quête :

<img class="alignnone" src="/images/solarus-launcher.png" alt="" width="594" height="413" />

Ce lanceur (ou launcher) vous permet de choisir la quête que vous voulez exécuter, et de configurer plusieurs options. Mais c'est loin d'être la seule nouveauté :
<ul>
	<li>Les maps peuvent maintenant avoir plus de 3 couches.</li>
	<li>Redimensionnement intelligent ! Vous pouvez maintenant redimensionner une salle entière d'un seul coup.</li>
	<li>Redimensionnement directionnel ! Les entités peuvent maintenant être étirées vers la gauche ou le haut, plus seulement vers la droite et le bas.</li>
	<li>Les musiques et les sons peuvent maintenant être joués depuis l'éditeur de quêtes !</li>
	<li>Le moteur supporte maintenant les boucles personnalisées dans les musiques OGG.</li>
	<li>Une console vous permet d'exécuter du code Lua à la volée dans l'éditeur de quêtes.</li>
	<li>La quête initiale contient maintenant de nombreux sprites, musiques et sons grâce à Diarandor et Eduardo.</li>
	<li>Performances améliorées lors de l'édition ou de l'exécution de maps avec beaucoup d'entités.</li>
	<li>La caméra est maintenant une entité de la map, vous pouvez personnaliser sa taille et son mouvement facilement.</li>
</ul>
Liste complète des changements : ici pour la <a href="http://www.solarus-games.org/2016/07/27/solarus-1-5-released-now-with-a-quest-launcher/">version 1.5.0</a> et <a href="http://www.solarus-games.org/2016/11/29/bugfix-release-1-5-1-and-improved-sample-quest/">ici pour la version 1.5.1</a> toute fraîche

Télécharger <a href="http://www.solarus-games.org/downloads/solarus/win32/solarus-1.5.1-win32.zip">Solarus 1.5.1 + l'éditeur de quêtes</a> pour Windows
<ul>
	<li>Télécharger le <a href="http://www.solarus-games.org/downloads/solarus/solarus-1.5.1-src.tar.gz">code source</a></li>
	<li><a href="http://www.solarus-games.org/">Blog de développement Solarus</a></li>
	<li>Comment <a href="http://wiki.solarus-games.org/doku.php?id=migration_guide#solarus_14_to_solarus_15">convertir votre quête</a> de Solarus 1.4.x vers Solarus 1.5.x</li>
</ul>
<h3>Tutoriels vidéo et live-codings</h3>
Solarus Quest Editor a tellement changé depuis mes premiers tutoriels que je les ai recommencés entièrement ! Pour le moment, 33 épisodes de niveau basique sont disponibles en français et en anglais, et j'en publie un nouveau chaque samedi. Les prochains vont être de niveau intermédiaire et avancé, je vous réserve des exemples de plus en plus évolués pour la suite ! J'essaie de rendre les tutos les plus indépendants possibles les uns des autres pour que vous puissiez directement aller à ceux qui vous intéressent.
<ul>
	<li><a href="https://www.youtube.com/playlist?list=PLzJ4jb-Y0ufzB4nXkSINFhbrtLlnlI4li">Tutoriels Solarus en vidéo</a></li>
	<li><a href="https://www.youtube.com/channel/UCB-tYDasLr9riVLh3DgiGgw">Chaîne YouTube ChristophoZS</a></li>
	<li><a href="https://twitter.com/ChristophoZS">Twitter @ChristophoZS</a> : abonnez-vous pour ne rien rater !</li>
</ul>
Je fais aussi de temps en temps des sessions de live-coding ou de live-making, c'est-à-dire des streamings vidéo de mon écran pendant que je développe Solarus, Solarus Quest Editor mais surtout aussi mes nouveaux projets de jeux : Zelda Mercuris' Chest, Zelda Oni-Link Begins Solarus Edition et plus. Il n'y a pas de planning régulier pour les streamings (contrairement aux tutos du samedi) mais les streamings sont toujours annoncés à l'avance sur mon fil <a href="https://twitter.com/ChristophoZS">Twitter @ChristophoZS</a>.

Ce qui est sûr c'est que les tutos aussi bien que les live-codings ont beaucoup de succès et que leur activité ne va pas se calmer !