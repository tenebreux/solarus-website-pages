<p>Je vous avais promis dans la newsletter une annonce importante concernant Zelda M'C... Eh bien la voici : nous 

allons faire une nouvelle version de la démo ! Ce sera exactement la même démo, mais avec le moteur du jeu complet, 

qui comporte déjà pas mal d'améliorations :</p>

<p><ul type="disc">
<li>Moteur de déplacement amélioré et plus maniable</li>
<li>Possibilité de porter et de lancer des bombes</li>
<li>Sous-écrans de pause identiques à ceux de la version complète</li>
<li>Pas de scrolling pour passer d'une zone à une autre de la map</li>
<li>Possibilité de configurer les touches du clavier</li>
<li>Possibilité de jouer avec une manette et de configurer ses boutons</li>
<li>Possibilité de choisir entre le mode plein écran et le mode fenêtré</li>
<li>Pas de plantages intempestifs :)</li>
</ul></p>

<p>Sans parler du nouvel écran-titre :</p>

<p align="center"><img src="/images/001.png" width="320" height="240"></p>

<p>Cette démo version 2.0 devrait sortir dans pas trop longtemps, au premier semestre 2005 :)</p>

<p>Joyeux Noël à tous !</p>