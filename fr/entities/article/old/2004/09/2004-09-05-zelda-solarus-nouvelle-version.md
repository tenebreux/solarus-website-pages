<p>Cette nouvelle version était secrètement en projet depuis plusieurs mois déjà. Comme vous pouvez le voir, ce design que nous appelerons la version 7.1 est inspiré du précédent. Il est l'oeuvre de @PyroNet, ce n'est que son premier design mais vous pouvez déjà admirer tout son talent !</p>

<p>Nous avons voulu faciliter la navigation en rétablissant la liste des jeux dans la colonne de gauche pour plus de clarté pour les nouveaux visiteurs. Nous avons aussi mis la zone "A l'affiche" en haut du design, en y ajoutant des liens relatifs aux dernières nouvelles ou aux rubriques importantes du site. Ces liens seront bien entendu régulièrement modifiés en fonction de l'actualité et des mises à jour.</p>

<p>Sur la page d'accueil, une colonne a été ajoutée à droite avec la newsletter, le sondage et les dates de sortie des prochains Zelda.</p>

<p>Nous espérons que vous apprécierez cette nouvelle version. N'hésitez pas à nous donner votre avis ou à nous signaler des bugs en laissant des commentaires !</p>