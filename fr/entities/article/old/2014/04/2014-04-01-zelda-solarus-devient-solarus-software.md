Depuis plus de 10 ans d'existence, la communauté Zelda Solarus a toujours été en perpétuelle évolution.

C'est donc tout naturellement que l'équipe est fière de vous dévoiler Solarus Software, l'agence de création de jeux amateurs !
<p style="text-align: center;"><img class="aligncenter" alt="Solarus Software" src="/images/logo-solarus-software-small2.png" width="314" height="114" /></p>
Avec une approche plus rationnelle et plus professionnelle du développement de jeu amateur, Solarus Software vous propose de prendre en charge les différentes étapes de la création de votre projet. Des spécifications à la commercialisation, nous proposons de vous accompagner tout au long de votre parcours.

Aujourd'hui plus que jamais, nous poursuivons donc notre évolution en nous dirigeant vers de nouveaux horizons. Si vous êtes intéressé(e) par la création d'un jeu amateur, contactez Solarus Software. Notre équipe de professionnels vous proposera une étude personnalisée adaptée à vos besoins et un projet sur mesure.

La création de jeux amateurs n'a jamais été aussi simple. Faites confiance à une équipe de passionnés qui n'a qu'un seul but : satisfaire ses clients !
<ul>
	<li><a href="http://www.zelda-solarus.com/soso/">En savoir plus</a></li>
	<li><a href="http://www.zelda-solarus.com/soso/presentation.php">Qui sommes-nous ?</a></li>
	<li><a href="http://www.zelda-solarus.com/soso/index.php">Nos réalisations</a></li>
</ul>
EDIT : Poisson d'avril ! Non, Zelda Solarus ne s'est pas encore transformé en une multinationale aux formules stéréotypées et aux slogans vides de sens.

<a href="http://www.zelda-solarus.com/soso"><img class="aligncenter" alt="" src="/images/corporate_rires.jpg" width="448" height="311" /></a>

&nbsp;

Un énorme bravo à Neovyse qui a assuré l'essentiel de la réalisation de ce site caricatural et a dessiné les magnifiques logos et boîtes de nos produits. Merci à toute l'équipe du forum pour leurs idées et à Daru13 pour son aide concernant la page recrutement :) Nous, on s'est bien marré en écrivant toutes ces bêtises. À l'année prochaine !