<p>Nous savons que vous demandez beaucoup d'infos sur ZAP mais malheureusement, il est très difficile pour nous de vous offrir des captures d'écrans ou d'autres infos sans vous dévoiler une bonne partie du jeu ce qui gâcherait ainsi la surprise...</p>
<p>Néanmoins, nous vous proposons un petit bilan de ce qu'il ne faut pas manquer sur ce jeu prévu pour cet été.</p><p>
- <a href="http://www.zelda-solarus.net/jeux.php?jeu=zf&zone=scr">Images du jeu (images non définitives)</a><br>
- <a href="http://www.zelda-solarus.net/jeux.php?jeu=zf&zone=anniv">Le dossier anniversaire de ZS</a><br>
- <a href="http://www.zelda-solarus.net/jeux.php?jeu=zf&zone=team">Les membres de l'équipe</a><br>
- <a href="http://www.zelda-solarus.net/jeux.php?jeu=zf&zone=power">Les performances du logiciel utilisé</a></p>
<p>En ce qui concerne la démo du jeu, vous n'aurez donc plus aucune info jusqu'à l'annonce de la date de sortie. Ensuite, de nombreuses choses risquent d'arriver à propos du jeu complet comme : l'utilisation de musiques entièrement nouvelles et orchestrées avec soin à la manière de The Wind Waker...</p>