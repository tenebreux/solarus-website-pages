<p>Le projet d'une version débugguée de Zelda : Mystery of Solarus, annoncé au mois de juillet dernier, avait été peu à peu laissé à l'abandon pour donner la priorité à Zelda : Advanced Project. Mais vous avez été nombreux à nous la réclamer ou à nous reprocher les nombreux bugs du jeu, alors j'ai finalement décidé de terminer ce projet.</p>

<p>Les abonnés à la <a href="http://www.zelda-solarus.com/newsletter.php">newsletter</a> sont déjà au courant, cette version débugguée est sur le point d'être terminée : nous procédons actuellement aux derniers tests.</p>

<p>38 corrections de bugs ou améliorations ont été faites, grâce en grande partie à ceux qui nous ont signalé les bugs sur le forum. Voici quelques uns des principaux changements :</p>

<p><ul type=disc>
<li>On pouvait marcher sur les trous, les tapis roulants et les autres événements se produisant au contact du héros, en mettant pause juste pendant le déplacement. C'était le plus gros bug du jeu, qui permettait de tricher un peu partout et qui pouvait ainsi provoquer plein d'autres bugs.</li>
<li>On pouvait tricher avec la plume et passer par exemple au-dessus des barrières, en avançant à un moment précis du calcul. Et à deux endroits du jeu on pouvait même passer à travers tous les murs à cause de ce bug.</li>
<li>Les ennemis pouvaient parfois pousser Link sur des trous (entre autres) sans qu'il tombe dedans.</li>
<li>Ce n'était pas vraiment un bug mais ça a énervé beaucoup de monde : dans le niveau 7, dans la salle avec les 5 portes à ouvrir en temps limité, les chronos ont été augmentés car c'était vraiment trop difficiles.</li>
</ul></p>

<p>La liste complète et mieux détaillée des bugs corrigés est affichée sur le forum, dans le topic <a href="http://zelda-solarus.consolemul.com/forums/viewtopic.php?t=224&start=200" target="_blank">"Les bugs"</a>.</p>

<p>Voilà, cette version débugguée sortira probablement dans quelques jours, avant la date de sortie de la démo de ZAP si tout se passe bien. Profitez-en pour y rejouer si vous n'êtes pas arrivé à la fin !</p>