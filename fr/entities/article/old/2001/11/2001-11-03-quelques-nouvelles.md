<p>Salut à tous !!</p>

<p>Ces (trop) courtes vacances ont été très profitables à la programmation de Zelda : Mystery of Solarus. En effet toutes les étapes entre les donjons 5 et 6 sont terminées ! Mais ne croyez pas pour autant que ce sera du facile... Enfin je ne vous en dis pas plus.</p>

<p>J'ai même déjà attaqué le sixième donjon. Vous aurez droit à quelques screenshots très bientôt :)</p>