<p align="center"><a href="/images/d5-1.jpg"><img border="0" src="/images/d5-1.jpg" width="160" height="120"></a>
<a href="/images/d5-2.jpg"><img border="0" src="/images/d5-2.jpg" width="160" height="120"></a>
<a href="/images/d5-3.jpg"><img border="0" src="/images/d5-3.jpg" width="160" height="120"></a></p>
<p><font face="Verdana" size="2">Voici des screenshots exclusifs du donjon 5 ! Inutile de vous dire que la difficulté de ce niveau est haute ! Christopho innove pour le mini-boss de ce donjon et les énigmes sont toujours aussi difficiles !</font></p>
<p><font face="Verdana" size="2">J'ai toujours pas élucidé l'énigme du mystérieux bouton gris au mur de la salle de la deuxième capture (cliquez pouragrandir) et encore plein de mystères planent dans ce niveau !</font></p>
<p><font face="Verdana" size="2">Qu'est-ce qui vous attend aussi ? De la rapidité, de l'espionnage (un peu comme sur N64), de la réflexion, de la patiente, enfin ; tout ce qui fait un zelda quoi !</font></p>
<p><font face="Verdana" size="2">Pendant les vacances, je vous montrerait aussi des screenshots exclusifs du donjon 9 qui est en préparation... ^_^</font></p>
<p><font face="Verdana" size="2">D'ici là, je pense que chris vous fera baver avec des infos supplémentaires sur le forum où ailleurs ! N'hésitez pas à y faire un tour de temps en temps !</font></p>
<p><font face="Verdana" size="2">Netgamer (<a href="http://www.zelda-solarus.com/mailto:(net_gamer@hotmail.com">net_gamer@hotmail.com</a>)</font></p>