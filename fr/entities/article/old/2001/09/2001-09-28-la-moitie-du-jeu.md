<p>La très symbolique barre de la moitié du jeu est sur le point d'être franchie ! Et oui, comme vous le savez le donjon 5 est en pleine phase de programmation. Or 10 donjons sont prévus donc le calcul est facile...</p>

<p>C'est l'occasion de faire un petit rappel de l'historique de Zelda : Mystery of Solarus et de son site officiel.</p>

<ul type=disc>
<li><font class="GrasItalique">Janvier 2001</font> : je découvre un logiciel appelé RPG Maker 2000, permettant de créer facilement des RPG. C'est là que j'ai l'idée d'un projet complètement fou, celui de créer un nouveau Zelda !!!</li>
<li><font class="GrasItalique">3 Février</font> : j'annonce officiellement le développement du jeu Zelda : Le Mystère du Solarus sur <a href="http://www.consolespower.fr.st" target="_blank">Consoles&nbsp;Power</a>.</li>
<li><font class="GrasItalique">9 Février</font> : la première démo (et toujours la seule existante !) est disponible.</li>
<li><font class="GrasItalique">de Février à fin Juin</font> : pas beaucoup de nouvelles infos publiées, même si la démo rencontre déjà un bon succès et même si le jeu progresse beaucoup techniquement (merci à Netgamer !).</li>
<li><font class="GrasItalique">4 Juillet 2001</font> : au vu du succès rencontré par la démo, ouverture officielle du mini-site et du forum dédiés au jeu ! A partir de ce moment-là, les mises à jour deviennent quasi-quotidiennes.</li>
<li><font class="GrasItalique">3 Août 2001</font> : le mini-site se détache de Consoles Power et devient un vrai site à part entière !</li>
</ul>

<p>Je remercie chaleureusement toutes les personnes qui m'ont encouragées à développer le jeu et sans qui le projet aurait pu être laissé à l'abandon. C'est vraiment grâce à votre soutien que j'ai la force de continuer. Si si c'est vrai :-)</p>

<p>Pour ce qui est d'une date de sortie finale, et bien cela commence à se préciser même si rien n'est fixé. Une sortie pour le printemps 2002 semble se dessiner à l'horizon...</p>