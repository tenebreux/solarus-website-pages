<p>Salut à tous !</p>

<p>L'ancien niveau 8 me plaisait pas du tout, on voyait rien, le chipset comportait plein d'erreur, c'était l'horreur quoi ! C'est pourquoi j'ai tout recommencé aujourd'hui et j'ai travaillé d'arrache-pied pour refaire un chipset et le niveau 8.</p>

<p>On ne vous dit rien sur ce donjon malheureusement sinon, on vous dévoilerait trop de détails sur le scénario... Cette partie du jeu est la plus palpitante car on approche de la fin et d'un affrontement tant attendu. Le jeu ne comporte que 9 donjons mais longs et progressivement difficiles de quoi vous occuper...</p>

<p>Trêve de bavardages, voici les écrans en question (cliquez dessus pour agrandir) :</p>

<table border="0" cellpadding="15" cellspacing="0" align="center">
<tr align="center">
<td><a href="/images/zs-n8-01.gif" target="_blank"><img src="/images/zs-n8-01.gif" width="160" border="0"></a></td>
<td><a href="/images/zs-n8-02.gif" target="_blank"><img src="/images/zs-n8-02.gif" width="160" border="0"></a></td>
</tr>
<tr>
<td><a href="/images/zs-n8-03.gif" target="_blank"><img src="/images/zs-n8-03.gif" width="160" border="0"></a></td>
<td><a href="/images/zs-n8-04.gif" target="_blank"><img src="/images/zs-n8-04.gif" width="160" border="0"></a></td>
</tr>
</table>