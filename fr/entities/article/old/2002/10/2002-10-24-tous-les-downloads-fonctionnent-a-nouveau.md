<p>Salut à tous !</p>

<p>Les downloads sont à nouveau disponibles. Je viens de les tester et ils fonctionnent tous ! Donc vous pouvez vous rendre sur la page <a href="http://www.zelda-solarus.com/download.php">downloads</a> pour télécharger Zelda Solarus ou d'autres fichiers divers. Nous les avons changé de serveur pour que le téléchargement soit plus rapide.</p>

<p>Le site n'est pas encore totalement fonctionnel (certaines images manquent) mais nous faisons tout notre possible pour y remédier rapidement. Merci de votre compréhension ;)</p>