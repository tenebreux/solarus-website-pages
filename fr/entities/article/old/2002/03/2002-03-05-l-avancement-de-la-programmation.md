<p>Salut à tous !</p>

<p>La programmation est de plus en plus compliquée à effectuer, car j'avoue que Netgamer me donne du fil à retordre avec les énigmes qu'il a conçues pour le niveau 8 qui est aussi compliqué que gigantesque ! Mais après un travail acharné depuis la semaine dernière, dont plus de six heures de travail rien qu'aujourd'hui, je peux dire que j'ai bien avancé et je peux même vous annoncer que le niveau 8 est quasiment terminé. Il me reste encore deux ridicules salles à programmer, ainsi que les boss, avant de tester l'ensemble du donjon.</p>

<p>Je prévois donc de boucler le donjon pour vendredi, date à laquelle j'espère donc recevoir de la part de Netgamer le niveau 9, également conçu ses soins.</p>

<p>Une fois les deux donjons terminés, il restera encore quelques maps à programmer et quelques détails à modifier, mais rien de vraiment long. La date du 26 mars devrait donc être à peu près respectée pour envoyer le jeu complet aux testeurs. Bref, tout sera prêt pour la date du vendredi 26 avril !</p>