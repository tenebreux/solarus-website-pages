J'écris cette news pour faire un point sur les nouveautés car il s'est passé beaucoup de choses aujourd'hui. Dans le but de redonner vie au site, nous avons fait trois grands changements :
[list]
[li]Nouveau design du site, avec aspect de Blog donc plus interactif[/li]
[li]Nouveau design du forum, plus attrayant qu'avant[/li]
[li]Recrutement d'un nouveau membre dans l'équipe : Marco[/li]
[/list]

Ces grands boulversements ont provoqué des émeutes sur le forum, heureusement tout est rentré dans l'ordre depuis l'intervention des Zoras. En revanche, seul point noir de la journée, le projet Zelda : Mercuris' Chest a réellement été perdu dans la bataille. Nous vous donnerons de plus amples informations ultérieurement.

En tout cas, nous espérons que ces changements vous plairont à long terme, même s'ils peuvent paraître surprenant au premier abord :)