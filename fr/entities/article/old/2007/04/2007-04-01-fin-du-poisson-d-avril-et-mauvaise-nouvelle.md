Nous avons rétabli le site et le forum à la normale. Bien sûr, tout le monde a vite compris que la version &quot;blog&quot; du site et le nouveau look du forum n'était qu'une grosse blague de premier avril :D
De même que toutes les news plus ou moins débiles qui ont été publiées aujourd'hui. Presque tout était bien entendu faux. Je tiens à remercier tous celles et ceux qui ont participé à la mise en place de l'animation de cette journée :
[list]
[li]Couet pour les fantastiques boutons et grades du forum[/li]
[li]Binbin pour les nouvelles couleurs du forum[/li]
[li]Marco pour ses news croustillantes sur Zelda[/li]
[li]Geomaster pour avoir le faux screen de Zelda Phantom Hourglass[/li]
[li]Noxneo et Seb le Grand pour leurs deux news improvisées[/li]
[li]Binbin pour avoir dégoté la photo de l'ordi qui brûle[/li]
[li]Mymy pour l'idée des Zoras colorés[/li]
[li]Super Tomate pour son apparition en guest star[/li]
[li]Tous les modérateurs pour leur participation aux événements sur le forum[/li]
[/list]
Merci enfin à tous les visiteurs, en particulier ceux du forum qui ont supporté le design atroce et le flood des Zoras colorés toute la journée. Merci à vous et bravo pour votre courage !

Je passe maintenant à la mauvaise nouvelle du jour. Il s'agit de la seule chose d'aujourd'hui qui n'était pas une blague : [b]l'annulation du projet Mercuris' Chest[/b]. Ok, je n'ai pas brûlé mon ordi, mais il n'empêche que le projet, lui, est vraiment annulé.
Mais je pense que ce n'est une surprise pour personne. Quand un projet n'avance pas pendant des mois et des mois, il faut se poser des questions et regarder la vérité en face. Ces questions, nous nous les sommes posées, et nous en sommes arrivés à la seule conclusion possible : l'annulation du projet. Ca ne sert à rien de le repousser indéfiniment. Le jeu était sans doute trop ambitieux.
Ne soyez donc pas trop déçus. L'immense majorité des projets de jeux amateurs n'aboutissent pas. Nous ne sommes pas des surhommes et aujourd'hui, nous sommes aussi victimes de ce phénomène. Ce qui à mon sens renforce la performance accomplie par le projet Mystery of Solarus qui lui est arrivé à son terme.

J'ai donc choisi d'annoncer l'annulation aujourd'hui, le 1er avril (même si techniquement, on est déjà le 2 avril là). Je ne sais pas si c'était la meilleure chose à faire, mais j'ai pensé que la mauvaise nouvelle passerait mieux un jour comme aujourd'hui. Tout le monde n'y croira pas tout de suite, donc l'annonce sera moins brutale à avaler.

[center][img]http://www.zelda-solarus.com/images/zf/artworks/link2_mini.jpg[/img][/center]

Zelda Solarus prend donc aujourd'hui un nouveau tournant. Avec l'abandon du projet, nous pourrons nous concentrer désormais à 100% sur ce qui fait aussi le succès du site : en particulier les mangas et les soluces complètes.

Bon, allez, c'est pas la fin du monde non plus ! Malgré tout, ce projet reste une réussite. Nous avons réussi à faire une démo (suivie de sa réédition) d'une qualité rarement égalée dans les autres Zelda amateurs, et qui a été téléchargée 50 000 fois. Je suis heureux d'avoir développé ce projet pendant ces dernières années. Déçu que ça s'arrête, mais en même temps je pense que comme moi, tout le monde s'y attendait un peu.