Bonjour à tous.

Amorcée par moi-même il y a quelques semaines, fortement aidée par Noxneo depuis peu et soutenue par l'ensemble de l'équipe d'administration et de modération, voici enfin la tant attendue réforme de la partie &quot;Jeux amateurs&quot; du forum Zelda-Solarus !

Cette réforme fait suite à un vieux débat au sein de l'équipe dont on ne saurait retrouver l'origine. Une première pseudo-réforme avait été effectuée par la séparation des topics en plusieurs catégories et par l'instauration d'un nouveau réglement concernant la création des topics en question. Mais ces règles ont très vite prouvé leurs limites...

Il y a quelques semaines, j'ai décidé de prendre les choses en main, ne supportant plus de voir tous ces topics destinés uniquement à être fermés aussitôt après avoir été ouverts. Mais j'étais également pris d'envies pédagogiques et il était donc question de non seulement réformer le forum à ce niveau mais également d'aider au mieux les membres qui se lancaient dans la création d'un jeu.

Comme vous avez sans doute pu le remarquer, la section &quot;Jeux amateurs&quot; a disparu du forum depuis ce matin ! La raison est fort simple : nous sommes en train d'appliquer cette réforme. En attendant, le nouveau réglement a été posté dans un forum dédié pour que vous puissiez en prendre connaissance dès maintenant. Si vous avez des questions, n'hésitez pas à nous en faire part.

Lorsqu'elle sera terminée, cette section aura une meilleure structure et surtout une toute nouvelle façon de fonctionner qui sera précisément décrite dans un tout nouveau réglement présent dans chaque forum de la section. Elle sera globalement mieux adaptée au processus de développement d'un jeu et d'un projet.

Certains d'entre vous grinceront sans aucun doute des dents car qui dit réforme totale des forums dit également déplacements voire suppressions de topics car c'est aussi là l'un des intérêts de la réforme : faire le ménage dans ces forums qui sont rapidement devenus de vraies poubelles. Vu la quantité de travail qui nous attend, la réforme devrait mettre plusieurs jours au pire à se matérialiser. Si votre topic a été supprimé, il est inutile de venir se plaindre auprès de l'équipe. Il est peut-être en cours de traitement par les modérateurs. Le cas échéant, dites-vous simplement que votre projet n'avait pas les conditions requises. Vous pouvez alors très bien poster dans l'un des nouveaux forums pour demander l'avis des membres, de l'aide voire remplir le questionnaire pour qu'un vrai topic soit ouvert sur votre projet.

[b]EDIT :

Toute l'équipe de modération et d'administration de Zelda-Solarus se joint à moi pour vous dire que la réforme est enfin visible sur le site.

Cette réforme est le fruit d'une longue réflexion et je souhaite remercier à ce sujet principalement Noxneo qui y a grandement contribué. Mais je remercie bien évidemment toute l'équipe pour son soutien et pour les débats animés (et les blagues pourries de Marco) tout au long de ce processus loin d'être évident.

Bien que visible, la réforme n'est pas encore terminée. Le réglement peut encore être légèrement modifié dans les jours à venir et nous sommes tout logiquement ouverts à toute remarque éventuelle.

Nous espèrons que cette nouvelle découpe du forum et que ce nouveau principe de fonctionnement vous satisfairont.

Longue vie à Zelda-Solarus ^^[/b] 