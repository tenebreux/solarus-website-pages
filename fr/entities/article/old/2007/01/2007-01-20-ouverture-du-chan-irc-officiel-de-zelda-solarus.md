Le chan IRC #ZS à ouvert sur Epik Network !

Pour ceux qui ne le savent pas encore, un chan IRC est un salon de discussion instantané auquel n'importe qui peut accéder. Pour cela, il suffit d'avoir un moyen de se connecter au chan à l'aide du protocole IRC (internet relay chat). Le chan permet à tous de discuter avec les autres memnbres du site, que ce soit de making, de Zelda, ou autre.

Si vous voulez des détails sur comment vous connecter au chan, cliquez sur &quot;Lire la suite...&quot;

P.S.: Nous rappellons que si vous voulez parlez exclusivement de Zelda, le chan #Zelda est ouvert. De même, le chan #rpg-maker est là pour toute aide sur RPG Maker.

[url=http://forums.zelda-solarus.com/index.php/topic,16126.msg259255.html#msg259255]Lire la suite...[/url]