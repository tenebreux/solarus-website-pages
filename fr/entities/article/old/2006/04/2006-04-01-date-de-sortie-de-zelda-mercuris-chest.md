Salut à tous :D

Nous avons beaucoup travaillé sur le projet Zelda M'C ces dernières semaines. Nous avons bossé sur l'intro et sur les premières maps que Link visitera au cours de son aventure. Ca avance plutôt très bien. Compte-tenu de la très longue durée de la quête, j'avais calculé que le jeu pourrait sortir au mieux le 26 avril 2013. Mais c'était trop beau. Malheureusement, la sortie ne pourra pas se faire si tôt. En effet, beaucoup de joueurs sont très superstitieux et ne préfèrent pas qu'il y ait le chiffre 13 dans la date. C'est pourquoi je vous annonce officiellement qu'il sera légèrement retardé.

Zelda : Mercuris' Chest sortira donc le [b]Mercredi 26 Avril 2023 à minuit[/b] heure française (sous réserve que nous soyons encore là ;)).

Si je fais ça, c'est dans votre intérêt. J'aurais sincèrement préféré que le jeu puisse sortir en 2013. Personnellement, je ne suis pas superstitieux, ça porte malheur.

Cette news sera mise à jour régulièrement au cours de la journée avec des nouvelles infos croustillantes sur le projet, ainsi que des images et une première vidéo du jeu. Donc pensez à revenir dans la journée :)