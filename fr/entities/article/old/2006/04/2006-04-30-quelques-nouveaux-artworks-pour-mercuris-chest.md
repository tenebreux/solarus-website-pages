Puisque l'actualité Zeldaesque est plutôt calme ces temps-ci, on s'est dit qu'il était temps de publier de nouveaux artworks. Il m'en reste d'autres en réserve, mais ils ne sont pas finis.

[url=http://www.zelda-solarus.com/jeux.php?jeu=zmc&amp;zone=artworks]Accéder aux artworks[/url]

Voici une jolie madame Gerudo très proche de celle d'Ocarina of Time, et un Goron, dont la posture ne vous rappellerait-elle pas notre cher ami gaulois amateur de sangliers ?
Je publie aussi le dessin du capitaine des Pirates, un peuple plutôt barbare habitant l'Est du pays.

A bientôt de nouveaux artworks !

[center][url=http://www.zelda-solarus.com/images/zf/artworks/pirate.jpg][img]http://www.zelda-solarus.com/images/zf/artworks/pirate_mini.jpg[/img][/url][/center]