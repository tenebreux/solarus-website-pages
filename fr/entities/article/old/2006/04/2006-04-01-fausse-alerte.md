Fausse alerte !

J'ai du nouveau ! Tout d'abord je tiens à vous rassurer, la fin de Zelda Solarus n'est pas encore arrivée. Je vous présente toutes mes excuses pour l'indisponibilité du site au cours de la dernière heure. Le site est désormais totalement rétabli.

Depuis que j'ai reçu le mail de notre hébergeur demandant la fermeture du site, c'est un peu la panique dans l'équipe. Evidemment, nous ne nous sommes pas laissés faire. J'ai passé deux heures à négocier auprès de notre hébergeur pour tenter d'en savoir plus. Et finalement, ça a porté ses fruits, puisque nous avons pu découvrir qu'en réalité, ce n'est pas Nintendo qui a demandé à mon hébergeur de fermer le site. Quelqu'un s'est fait passer pour Nintendo afin de faire fermer Zelda Solarus.

Toute cette affaire n'était donc qu'un simple coup monté. Avec l'aide de l'hébergeur, du service de répression de la cyber-délinquance, de la CIA, du SD-6, de la Cellule Anti-Terroriste de Los Angeles et de la gendarmerie de Maubeuge, nous sommes parvenus a identifier le suspect. Je tiens à remercier tout particulièrement le coiffeur de Franck Leboeuf qui nous a mis sur la piste du coupable. Il s'agirait du cyber-terroriste bien connu Super Tomate. Super Tomate est activement recherché depuis de nombreuses années. Il s'est rendu coupable de quelques graves délits, notamment une orthographe déplorable et un avatar ridicule. Par ailleurs, il est surtout connu pour avoir écrit un blog raciste sur les pommes de terre et les lentilles. Enfin, il aurait créé une usine cybernétique de concombres génétiquement modifiés.

[center][img]http://esial.free.fr/Forum/images/avatars/538397355441f3a1b1ab39.jpg[/img][/center]

Des témoins auraient aperçu Super Tomate sur le forum. Nous vous tiendrons au courant de l'avancement de l'enquête dès que nous en saurons plus.
