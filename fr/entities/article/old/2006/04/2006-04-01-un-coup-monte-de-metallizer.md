Depuis environ une heure maintenant, Super Tomate est interrogée par le GLOUPS et d'autres organismes spécialisés dans les interrogatoires rapides et efficaces. Comme le montre cette image exclusive, Super Tomate semble avoir énormément souffert !

[center][img]http://www.zelda-solarus.com/images/uploads/st_arretee.jpg[/img][/center]

Mais tout cela porté ses fruits car nous venons d'apprendre que ce cyber-criminel a avoué avoir a été recruté par Metallizer pour effectuer cette vaste opération ! Une unité spéciale s'est lancée à la recherche de Metallizer, qui reste depuis peu introuvable. Nous vous tiendrons au courant dès que nous avons la moindre information.