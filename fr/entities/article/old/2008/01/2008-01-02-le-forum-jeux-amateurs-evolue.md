Bonjour à tous !

Tout d'abord, permettez-moi de vous souhaiter [b]une bonne et heureuse année 2008[/b], de la part de toute l'équipe de Zelda-Solarus :)
Que cette nouvelle année soit joyeuse et riche en émotions ^_^
Du côté de ZS, si 2007 a vu l'annulation du projet Zelda : Mercuris' Chest, 2008 devrait être ponctuée de meilleures nouvelles, nous l'espérons et nous travaillons dur pour vous proposer des surprises B)

Cette news est consacrée à une partie importante de l'activité du forum : [b]les jeux amateurs[/b]. Bien que notre site soit avant tout consacrée à Zelda, une large partie forum est dédiée à la création de jeux amateurs comme Zelda : Mystery of Solarus, que ce soient des Zelda ou non.

Il y a quelques mois, nous avions mis en place une réforme du forum Jeux Amateurs, qui avait plusieurs objectifs, dont celui de limiter les projets inintéressants. Nous avions pour cela établi un règlement relativement restrictif pour inciter les créateurs à présenter leurs projets avec un minimum de sérieux. En particulier, les auteurs de projets devaient répondre à un questionnaire complet pour montrer l'avancement de leur jeu.
Après quelques mois d'application, si cette réforme a eu l'effet escompté, elle a aussi malheureusement réduit le nombre de projets intéressants. La réforme était sans doute trop restrictive et nuisait à la créativité des développeurs, notamment à cause du questionnaire. De plus, les forums étaient découpés d'une manière peu intuitive.

Aujourd'hui, la réforme évolue :
[list]
[li]Les modérateurs chargés de gérer ce forum s'appellent désormais &quot;[b]Conseillers JA[/b]&quot; au lieu de &quot;Modérateurs JA&quot;. Ils sont plus là pour conseiller les créateurs que pour les modérer :)[/li]
[li]Les forums dédiés aux projets ont été renommés en &quot;Débuts de projets&quot; et &quot;Projets avancés ou terminés&quot; pour [b]plus de clarté[/b].[/li]
[li]Les membres sont [b]libres de poster leurs projets[/b] dans les deux forums selon les cas, et en les présentant de la manière qu'ils souhaitent. Il n'y a plus de questionnaire à répondre ;)[/li]
[/list]

Voilà, nous espérons que grâce à cette évolution, le forum conviendra mieux aux créateurs de jeux, et qu'en 2008, on y verra de nombreux projets prometteurs  ^_^