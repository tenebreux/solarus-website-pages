Vous avez peut-être constaté que ce week-end et ce matin, le site a connu quelques perturbations : il a été indisponible par moments. Rassurez-vous, ces dysfonctionnements n'ont rien d'anormal : ils ont été causés par quelques opérations techniques à faciliter un futur changement de serveur. L'objectif de cette news est de vous expliquer ce qui se passe : la situation technique et financière du site :)

[b]Les dons remplacés par une pub discrète[/b]

Comme vous le savez, l'hébergement de Zelda Solarus nous coûte assez cher car il nécessite un serveur relativement puissant. Jusqu'à présent, tout était financé par la soluce &quot;payante&quot; de Mystery of Solarus, avec un système Allopass. A présent, la soluce est de moins en moins consultée et elle ne suffit donc plus à financer le serveur. Il y avait aussi un système de dons depuis 2006, mais à part un don le jour de l'annonce, cela nous a rapporté très précisément 0 euros ^_^.
Pour y remédier, nous avons donc remplacé le lien &quot;faites un don&quot; par une publicité discrète, sous forme d'annonces textuelles Google, dans la colonne de gauche du site et également sur le forum. Il y avait déjà eu ce type de publicité sur Zelda Solarus par le passé, avant que l'on essaie les dons. Les dons n'ayant pas fonctionné, on remet donc la pub ;). Mais en aucun cas on ne mettra des bannières de pub animées, elles sont trop aggressives, et nous sommes comme vous : nous n'aimons pas la pub :P.

[b]Un déménagement imminent ![/b]

La deuxième mesure est une bonne nouvelle : comme annoncé plus haut, nous allons bientôt changer de serveur. Le serveur actuel a déjà quelques années. A l'époque où nous l'avions loué, il était puissant, et donc cher. Mais aujourd'hui, alors qu'il est toujours aussi cher, notre hébergeur nous propose de nouvelles offres de serveurs dédiés, moins chères et nettement plus puissantes. En clair, ça va être bénéfique pour tout le monde : pour nous car nous allons faire des économies, et pour vous car le serveur sera plus récent, plus puissant (bien que l'actuel ne pose pas de problèmes de rapidité) et aura nettement plus d'espace disque. Sur le nouveau serveur, nous pourrons par exemple installer un Wiki plus facilement, ce qui nous permettra d'héberger [url=http://wiki.zelda-solarus.com]Zeldapédia[/url] et donc relier cette encyclopédie un peu plus à Zelda Solarus :)

[b]Et donc, c'est pour quand ?[/b]

Concrètement, je pense effectuer le déménagement dans la semaine. Si tout va bien, le site restera entièrement disponible pendant l'opération. Si tout se passe bien, vous ne verrez même pas la différence :)
