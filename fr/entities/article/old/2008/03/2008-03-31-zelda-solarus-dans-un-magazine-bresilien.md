Oh, une mise à jour !
Le record a été battu, avec plus de deux mois sans mises à jour -_-
Je vous dois donc des explications. Croyez bien que c'est exceptionnel et que j'espère ne jamais rebattre ce triste record  ^_^
Il y a d'abord des raisons personnelles que je ne vais pas détailler, mais aussi le fait qu'en ce moment, il n'y ait pas beaucoup d'actu Zelda. De plus, l'équipe que nous sommes n'est pas toujours aussi disponible qu'avant. C'est pourquoi je songe à recruter de nouvelles personnes motivées, mais ça, j'en reparlerai dans une news ultérieure :)

Mais venons-en au sujet de cette mise à jour !
Maker World est un magazine brésilien sur le web, entièrement consacré à la création de jeux amateurs en particulier avec RPG Maker. Dans le numéro 3, toute une rubrique est consacrée à Zelda : Mystery of Solarus et Zelda : Mercuris' Chest. Pas moins de 4 pages parlent de nos créations, ainsi que la couverture du magazine ! Voici les images :

[center]
[url=http://www.zelda-solarus.com/images/actu/maker_world/1.png][img]http://www.zelda-solarus.com/images/actu/maker_world/1_mini.png[/img][/url]

[url=http://www.zelda-solarus.com/images/actu/maker_world/2.png][img]http://www.zelda-solarus.com/images/actu/maker_world/2_mini.png[/img][/url] [url=http://www.zelda-solarus.com/images/actu/maker_world/3.png][img]http://www.zelda-solarus.com/images/actu/maker_world/3_mini.png[/img][/url]

[url=http://www.zelda-solarus.com/images/actu/maker_world/4.png][img]http://www.zelda-solarus.com/images/actu/maker_world/4_mini.png[/img][/url] [url=http://www.zelda-solarus.com/images/actu/maker_world/5.png][img]http://www.zelda-solarus.com/images/actu/maker_world/5_mini.png[/img][/url]
[/center]

Comme vous le voyez, il y a deux pages consacrées à Mystery of Solarus et deux consacrées à Mercuris' Chest, avec plusieurs captures d'écran. L'article parle principalement du scénario des jeux et de leur création, mais comme le magazine est en portugais, j'ai du mal à tout comprendre ^^ Si vous connaissez cette langue et que vous souhaitez nous apporter votre aide en faisant la traduction, n'hésitez pas à me contacter via le forum, car cela intéressera tout le monde ^_^
A noter que les responsables de Maker World travaillent actuellement sur une traduction anglaise et comptent également publier une version française du magazine.

[list]
[li][url=http://bincworld.com/mw]Site officiel[/url] du magazine Maker World (téléchargez le numéro 3)[/li]
[li]Lire le sujet sur le [url=http://forums.zelda-solarus.com/index.php/topic,18955.msg325918.html#msg325918]forum[/url][/li]
[/list]