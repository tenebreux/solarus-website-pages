Le cadeau de Noël Solarus est arrivé !

Une nouvelle version de notre moteur de jeu Solarus et de l'éditeur de quêtes Solarus Quest Editor vient de sortir. Tout l'équipe y travaille d'arrache-pied depuis deux ans et demi.

<a href="../data/fr/entities/article/old/2018/12/images/quest_editor_screenshot-1024x576.png">
<img class="aligncenter size-medium wp-image-38365" src="/images/quest_editor_screenshot-1024x576-300x169.png" alt="" width="300" height="169" /></a>

Nous sommes très fiers de vous présenter Solarus 1.6, qui apporte énormément de nouvelle fonctionnalités aux créateurs de jeux, comme par exemple :
<ul>
 	<li>Utilisation d'<strong>OpenGL</strong> pour la partie graphique et support des<strong> shaders GLSL</strong> (merci à Std::gregwar et Vlag)</li>
 	<li>Support de l'<strong>éditeur de texte externe</strong> de votre choix. Intégration de <strong>Zerobrane</strong> : autocomplétion, points d'arrêt, inspection de la pile (merci à Std::gregwar)</li>
 	<li>Nombreuses améliorations de l'éditeur de map, avec notamment le <strong>générateur de contours</strong> (autotiles), le <strong>remplacement de tiles</strong> et le support des <strong>tilesets multiples</strong></li>
 	<li>Nombreuses améliorations de l'éditeur de tilesets, avec la <strong>sélection multiple</strong>, les motifs animés avec <strong>nombre de frames et délai au choix</strong></li>
 	<li>Nombreuses améliorations de l'éditeur de sprites</li>
 	<li>Possibilité d'<strong>importer</strong> des ressources d'autres quêtes</li>
 	<li>De magnifiques nouveaux <strong>tilesets libres</strong> (Zoria par DragonDePlatino, Ocean's Heart par Max Mraz)</li>
 	<li><strong>Nouvelles polices </strong>pixel libres (par Wekhter)</li>
 	<li>États du héros personnalisés pour un contrôle avancé du héros</li>
 	<li>Innombrables nouvelles fonctionnalités dans l'API Lua</li>
</ul>
Sachez aussi que Solarus 1.6 est entièrement compatible avec les quêtes 1.5. Aucune action particulière n'est nécessaire pour mettre à jour votre projet vers Solarus 1.6.
<ul>
 	<li><a href="http://www.solarus-games.org/engine/download/">Télécharger Solarus 1.6</a></li>
 	<li><a href="https://gitlab.com/solarus-games/solarus-alttp-pack/-/archive/v1.6.0/solarus-alttp-pack-v1.6.0.zip">Pack de ressources Zelda A Link to the Past</a> pour Solarus</li>
 	<li><a href="https://gitlab.com/solarus-games/solarus-free-resource-pack/-/archive/v1.6.0/solarus-free-resource-pack-v1.6.0.zip">Pack de ressources libres</a> pour Solarus</li>
</ul>
De nouveaux tutoriels vidéos vont arriver prochainement pour vous initier à la création de jeux avec Solarus.

Je vous laisse découvrir l'incroyable <a href="https://www.youtube.com/watch?v=vt07FwzLo9A">Trailer Solarus 1.6</a> réalisé par Olivier Cléro !

Joyeux Noël à toutes et à tous !