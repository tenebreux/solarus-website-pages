Cette semaine, nous avons beaucoup avancé sur les différentes <strong>grottes de Cocolint</strong>. cela commence vraiment à devenir très classe !
Du coup, voici une image de l'une d'entre elles.

<a href="../data/fr/entities/article/old/2018/03/images/grotte.png"><img class="alignnone size-medium wp-image-38306" src="/images/grotte-300x240.png" alt="" width="300" height="240" /></a>

N'hésitez pas à donner votre avis sur cette nouvelle image. :)