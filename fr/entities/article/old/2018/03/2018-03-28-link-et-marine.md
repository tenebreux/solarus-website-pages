Cette semaine, nous avons beaucoup travaillé sur la suite de la quête principale. Cela avance maintenant vraiment très vite !

Nous avons du coup pas mal bossé sur certaines cinématiques du jeu dont celle de Marine et Link qui jouent de la musique ensemble.

<a href="../data/fr/entities/article/old/2018/03/images/1-2.png"><img class="alignnone size-medium wp-image-38314" src="/images/1-2-300x240.png" alt="" width="300" height="240" /></a>
<div class="clearfix"></div>
De plus, ce soir, un live Making a lieu à 21h00 afin de vous montrer le travail réalisé. Cela se passe à cette adresse : <a href="https://www.youtube.com/watch?v=1wML92aOK1s">https://www.youtube.com/watch?v=1wML92aOK1s</a>
Si cela vous intéresse, n'hésitez pas à venir. L'émotion et la bonne humeur seront bien entendu de la partie !