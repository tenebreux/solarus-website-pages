Cette semaine, pour le projet de remake de Link's Awakening, on vous présente l'unique sorcière de Cocolint. Pour ceux qui ne connaissent pas, il s'agit d'un personnage important du jeu. :)

<a href="../data/fr/entities/article/old/2018/02/images/2-3.png"><img class="alignnone size-medium wp-image-38296" src="/images/2-3-300x240.png" alt="" width="300" height="240" /></a> 
<a href="../data/fr/entities/article/old/2018/02/images/1-3.png"><img class="alignnone size-medium wp-image-38295" src="/images/1-3-300x240.png" alt="" width="300" height="240" /></a>

Comme d'habitude, n'hésitez pas à nous laisser un petit commentaire.