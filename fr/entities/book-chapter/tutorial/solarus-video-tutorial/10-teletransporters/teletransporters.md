# {title}

[youtube id="9YyEpN0z1OY"]

## Sommaire

- Utiliser les destinations
- Des téléporteurs sur la même map
- Relier des maps avec des téléporteurs
- Relier des maps avec des téléporteurs à défilement
- Définir le nom du monde

## Resources

- Video réalisée avec Solarus 1.5.
- [Télécharger Solarus](/solarus/download)
- [Documentation de Solarus](https://www.solarus-games.org/doc/latest/)
- [Apprendre à programmer en Lua](http://www.lua.org/pil/contents.html)
- [Pack de resources pour Legend of Zelda: A Link to the Past](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)
