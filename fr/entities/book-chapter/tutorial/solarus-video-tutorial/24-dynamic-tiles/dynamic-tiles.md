# {title}

[youtube id="J2OssGIa828"]

## Sommaire

- Utiliser les tiles dynamiques
  - Faire apparaître une échelle dès lors qu'un interrupteur est activé
  - Vider l'eau dès lors qu'un interrupteur est activé

## Resources

- Video réalisée avec Solarus 1.5.
- [Télécharger Solarus](/solarus/download)
- [Documentation de Solarus](https://www.solarus-games.org/doc/latest/)
- [Apprendre à programmer en Lua](http://www.lua.org/pil/contents.html)
- [Pack de resources pour Legend of Zelda: A Link to the Past](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)
