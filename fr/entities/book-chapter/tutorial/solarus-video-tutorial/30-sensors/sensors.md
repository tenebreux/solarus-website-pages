# {title}

[youtube id="GCixuthVxtU"]

## Sommaire

- Utiliser les entités capteurs
  - Changer la position du héros quand un capteur est activé
  - Déplacer le héros vers l'entré de la pièce après qu'il soit tombé dans un trou (et sauvegarder sa position sur ce terrain solide)
- Différence entre les capteurs et les interrupteurs/téléporteurs

## Resources

- Video réalisée avec Solarus 1.5.
- [Télécharger Solarus](/solarus/download)
- [Documentation de Solarus](https://www.solarus-games.org/doc/latest/)
- [Apprendre à programmer en Lua](http://www.lua.org/pil/contents.html)
- [Pack de resources pour Legend of Zelda: A Link to the Past](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)
