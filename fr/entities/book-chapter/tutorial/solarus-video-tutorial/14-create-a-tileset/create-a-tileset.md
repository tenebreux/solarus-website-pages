# {title}

[youtube id="DGaunR-3em4"]

## Sommaire

- Comment créer une image de tileset dans [GIMP](https://www.gimp.org/), en utilisant les graphismes de la Caverne Flagello de *Legend of Zelda: Link's Awakening*
- Utiliser l'éditeur de tilset
- Changer de tileset sur une map (convention de nommage)

## Resources

- Video réalisée avec Solarus 1.5.
- [Télécharger Solarus](/solarus/download)
- [Documentation de Solarus](https://www.solarus-games.org/doc/latest/)
- [Apprendre à programmer en Lua](http://www.lua.org/pil/contents.html)
- [Pack de resources pour Legend of Zelda: A Link to the Past](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)
