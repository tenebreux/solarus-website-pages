# {title}

[youtube id="ftdaX9hrsrw"]

## Sommaire

- Créer un pont
  - Permettre au héros de passer au-dessus ou en-dessous grâce au système des couches
  - Astuce de la plate-forme invisible pour éviter de retomber trop tôt sur la couche du bas

## Resources

- Video réalisée avec Solarus 1.5.
- [Télécharger Solarus](/solarus/download)
- [Documentation de Solarus](https://www.solarus-games.org/doc/latest/)
- [Apprendre à programmer en Lua](http://www.lua.org/pil/contents.html)
- [Pack de resources pour Legend of Zelda: A Link to the Past](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)
