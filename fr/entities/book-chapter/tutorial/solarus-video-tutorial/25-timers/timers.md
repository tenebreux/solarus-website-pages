# {title}

[youtube id="9mJzkEl6YaE"]

## Sommaire

- Utiliser les timers pour ajouter un délai dans un script
  - Jouer un son 5 secondes après avoir activé un interrupteur
- Contexte de timer
- Répéter un timer
  - Indéfiniment
  - Seulement 3 fois
- Interrompre un timer
- Vider l'eau progressivement après qu'un interrupteur ait été activé

## Resources

- Video réalisée avec Solarus 1.5.
- [Télécharger Solarus](/solarus/download)
- [Documentation de Solarus](https://www.solarus-games.org/doc/latest/)
- [Apprendre à programmer en Lua](http://www.lua.org/pil/contents.html)
- [Pack de resources pour Legend of Zelda: A Link to the Past](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)
