# {title}

[youtube id="5eoK88SyYws"]

Cette vidéo répond à une question fréquente : comment scroller correctement entre les maps lorsqu'elles sont de tailles différentes. La solution est de simplement renseigner la propriété "Coordonnées" de chaque map !

## Sommaire

- Utiliser des téléporteurs à défilement sur des maps de tailles différentes
  - Définir les position des maps du monde
  - Corriger les problèmes de défilement de la caméra

## Resources

- Video réalisée avec Solarus 1.5.
- [Télécharger Solarus](/solarus/download)
- [Documentation de Solarus](https://www.solarus-games.org/doc/latest/)
- [Apprendre à programmer en Lua](http://www.lua.org/pil/contents.html)
- [Pack de resources pour Legend of Zelda: A Link to the Past](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)
