# {title}

[youtube id="fyQAQzhAzZQ"]

## Sommaire

- Créer une échelle
- Utiliser les entités jumpers
- Faire sauter le héros depuis une falaise (verticalement et en diagonale)
- S'assurer que le héros n'atterrisse pas dans un mur
- Créer une map de donjon à plusieurs niveaux
- Faire sauter le héros dans une map de donjon à plusieurs niveaux

## Resources

- Video réalisée avec Solarus 1.5.
- [Télécharger Solarus](/solarus/download)
- [Documentation de Solarus](https://www.solarus-games.org/doc/latest/)
- [Apprendre à programmer en Lua](http://www.lua.org/pil/contents.html)
- [Pack de resources pour Legend of Zelda: A Link to the Past](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)
