# {title}

[youtube id="ROfzXEj-cqs"]

Cet épisode montre comment créer un item qui représente les Rubis, avec 3 variantes : 1, 5 ou 20 rubis.

## Sommaire

- Créer le script d'objet pour les rubis
  - Assigner une ombre à l'objet
  - Jouer un son quand l'objet est ramassé
- Créer le dialogue pour le trésor de type rubis
- Différence entre les évènements d'objet `on_started()` et `on_pickable_created()`
- Utiliser les variantes d'objets

## Resources

- Video réalisée avec Solarus 1.5.
- [Télécharger Solarus](/solarus/download)
- [Documentation de Solarus](https://www.solarus-games.org/doc/latest/)
- [Apprendre à programmer en Lua](http://www.lua.org/pil/contents.html)
- [Pack de resources pour Legend of Zelda: A Link to the Past](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)
