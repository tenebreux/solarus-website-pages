# {title}

[youtube id="9kUsFuqtzSo"]

## Sommaire

- Utiliser l'éditeur de sprite
  - Animations
  - Directions
  - Frames
  - Définir l'origine
- Placer une entité PNJ et lui attribuer un sprite
- Différence entre sprites et entités
  - Sprite de cœur vide

## Resources

- Video réalisée avec Solarus 1.5.
- [Télécharger Solarus](/solarus/download)
- [Documentation de Solarus](https://www.solarus-games.org/doc/latest/)
- [Apprendre à programmer en Lua](http://www.lua.org/pil/contents.html)
- [Pack de resources pour Legend of Zelda: A Link to the Past](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)
- [Ressources utiles pour reproduire ce chapitre](https://www.solarus-games.org/tuto/fr/basics/ep16_ressources.zip)
