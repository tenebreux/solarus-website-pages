### Synopsis

ZeldoRetro a créé en 2017 un court jeu *The Legend of Zelda* pour défier son ami Adenothe. Ce n'était pas destiné à être publié publiquement. Cependant, la sortie du second chapitre l'a fait reconsidérer cette décision, car tout le monde lui disait "Attendez ? Si ceci est le chapitre 2, où est le chapitre 1 ?". Le voici !

Ce jeu est le premier épisode dans la série de ZeldoRetro *Le Défi de Zeldo*. Ce premier chapitre est intitulé *La Revanche du Bingo*, est est bien moins ambitieux que sa suite. Il ne contient qu'un seul donjon, et pas de monde à explorer, ce qui le rend plutôt court. Il se finit en moins d'une heure.
