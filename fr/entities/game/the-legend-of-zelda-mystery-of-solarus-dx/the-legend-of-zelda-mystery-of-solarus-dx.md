### Présentation

*The Legend of Zelda: Mystery of Solarus DX* est pensé comme la suite directe de *The Legend of Zelda: A Link to the Past* sur SNES, utilisant les mêmes graphismes et mécaniques de jeu. *Mystery of Solarus DX* est le premier jeu réalisé avec Solarus, et c'est bien pour ce jeu que le moteur a été créé au départ.

*Mystery of Solarus DX* est en fait un remake amélioré de la première création *Mystery of Solarus* (sans le suffixe *DX*). Ce premier jeu, développé avec RPG Maker 2000, a été publié en 2002 et était seulement disponible en français. Le projet *DX* a été dévoilé le 1er avril 2008 et est sorti en décembre 2011. Son objectif était de corriger les nombreuses limitations de son prédécesseur : le système de combats, les boss, l'utilisation des objets, etc.

Cependant, ce n'est pas tout, car de nouveau éléments graphiques et musicaux ont été spécialement créés pour vous accompagner tout le long du jeu. Cette version Deluxe est l'opportunité de revivre l'aventure originelle d'une manière nouvelle, ou bien de la découvrir pour la première fois si vous n'y aviez pas joué avant !

![Link](artworks/artwork_link.png "Link")

### Synopsis

Après les évènements de *A Link to the Past*, Ganon a été vaincu et emprisonné pour toujours dans la Terre d'Or par le roi d'Hyrule, grâce au sceau des Sept Sages.

![L'infâme Ganon](artworks/artwork_ganon.png "L'infâme Ganon")

Les années passèrent, et un jour le Roi fut atteint d'une étrange et grave maladie. Les docteurs et apothicaires de tout le royaume sont venus essayer de le soigner, mais en vain. Le roi ne survécut pas, et son pouvoir se volatilisa, fragilisant alors le sceau des Sept Sages.

![Sahasrahla l'ancien](artworks/artwork_sahasrahla.png "Sahasrahla l'ancien")

Le héros, sur les conseils de son maître Sahasrahla, fit confiance à l'héritière du trône : la princesse Zelda. Zelda, avec la Triforce, s'associa à huit mystérieux enfants pour éparpiller la Triforce en huit fragments de par le monde. La paix était alors maintenue dans le royaume… jusqu'au jour où débute votre épopée.

![La prrincesse Zelda](artworks/artwork_zelda.png "La princesse Zelda")

Découvrez une grande aventure pleine de donjons, de monstres, de personnages mystérieux, de labyrinthes et d'énigmes.

![Billy](artworks/artwork_billy.png "Billy")
