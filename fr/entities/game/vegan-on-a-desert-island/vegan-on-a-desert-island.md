### Présentation

Inspiré par la terrible question *"Que feraient des vegans s'ils étaient coincés sur une île déserte ?"*, Vegan on a Desert Island donne la réponse : **ils essayeraient de la réparer**.

![Rachel](artworks/artwork_rachel.png)

Dans cette aventure 2D remplie d'énigmes, cette hypothétique question est poussée à son paroxysme. Rempli de dialogues politiques insolents, de puzzles et d'une histoire profonde, Vegan on a Desert Island est le cocktail détonnant entre une esthétique rétro et l'absurdité de la culture post-moderne.

![Rachel](artworks/artwork_seagulls.png)

Des problèmes bien réels liés à la cause animale comme la pollution humaine, les prédateurs sauvages et la discrimination spéciste font partie des thèmes explorés par ce jeu. Vegan on a Desert Island est un jeu puissant qui promet de vous faire réfléchir.

### Synopsis

Insatisfaite par la banalité de sa vie, Rachel accepte de s'enfuir avec Marvin vers une île tropicale du Pacifique. Alors qu'il essayait de manœuvrer son bateau, le duo s'écrasa contre une falaise et fut emporté vers le rivage. Marvin mourut, et Rachel demeura seule pour s'aventurer sur l'île.

![Greybeard](artworks/artwork_greybeard.png)

En tant que vegan, Rachel peut parler aux animaux peuplant l'île. Leurs communautés sont gangrénées par des conflits que seule une intervenante extérieure comme Rachel peut résoudre. Elle doit sympathiser avec les villageois, explorer l'île et résoudre les énigmes qui se trouvent sur son chemin. En combattant des problèmes comme la pollution due à l'activité humaine et la souffrance des animaux sauvages, Rachel demeure optimiste et pense qu'elle pourra réparer le monde.

Cependant, travailler avec les autres n'est pas toujours si facile. Peut-elle réellement résoudre toutes les difficultés de l'île ?
