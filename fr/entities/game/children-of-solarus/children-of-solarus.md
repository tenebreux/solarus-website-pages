### Présentation

*Children of Solarus* est le remake 100% libre de *The Legend of Zelda: Mystery of Solarus DX*. Tout le contenu propriétaire est remplacé par du contenu 100% libre : sprites originaux, tilesets originaux, musiques originales, sons orginaux, etc. L'histoire et les personnages sont un peu étendus afin de permettre au jeu de se suffire à lui-même, au lieu de se baser sur la série *Zelda*.
