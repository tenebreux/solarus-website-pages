### Installez snap

Premièrement, assurez-vous d'avoir `snapd` installé. Si ce n'est pas le cas, installez le avec votre gestionnaire de paquets. Cela devrait donner quelque chose comme ceci :

```bash
sudo apt install snapd
```

### Installez Solarus

#### Depuis le terminal

Installez `solarus` depuis le terminal en utilisant snap.

```bash
sudo snap install solarus
```

#### Depuis la Logithèque

Solarus est également disponible dans la Logithèque. Cliquez sur "Installer" pour l'installer.

[![Snapstore](https://snapcraft.io/static/images/badges/en/snap-store-black.svg)](https://snapcraft.io/solarus)
