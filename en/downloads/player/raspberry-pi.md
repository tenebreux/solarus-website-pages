At the moment, the easiest way to get Solarus on Raspberry Pi is by using RetroPie.

1. Install (or update to) the latest version of [RetroPie](https://retropie.org.uk/docs/First-Installation/).
2. In the RetroPie main menu, select **Manage Packages** and then **Manage optional packages**.
3. Find the `solarus` package and select the **Update from binary** option.
4. Download and [place Quests](https://retropie.org.uk/docs/Transferring-Roms/) into the dedicated folder for Solarus, e.g. `~/RetroPie/roms/solarus`.
5. Restart your device and your Quests will be ready to launch from EmulationStation.
