### Install snap

First, ensure you have `snapd` installed. If not, install it with your package manager. It should be something like:

```bash
sudo apt install snapd
```

### Install Solarus

#### From the terminal

Install `solarus` from the terminal using snap.

```bash
sudo snap install solarus
```

#### From the Software Center

Solarus is also available in the Software Center. Click on "Install" to install it.

[![Snapstore](https://snapcraft.io/static/images/badges/en/snap-store-black.svg)](https://snapcraft.io/solarus)
