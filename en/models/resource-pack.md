[highlight]
[space thickness="20"]
[container]

[row]
[column width="1"]
[align type="center"]
[icon-solarus icon="resource_pack" size="64"]
[/align]
[/column]
[column_1 width="8"]
[row_1]

# {title}

[/row_1]
[row_2]

#### {authors}

[/row_2]
[/column_1]

[column_2 width="3"]
[align type="right]
[entity-button-download entity-type="resource-pack" id={id} label="Download" label-disabled="In development" modal-title="Download" modal-button-label="Download"]
[/align]
[/column_2]
[/row]

[/container]
[/highlight]
[container]
[space thickness="0"]

## Screenshots

[medias-gallery id="{id}"]

[space]

[row]
[column width="8"]

## Description

{content}

[/column]
[column width="4"]

## Details

[model id="resource-pack-sheet" context-id="{id}"]
[/column]
[/row]
[/container]