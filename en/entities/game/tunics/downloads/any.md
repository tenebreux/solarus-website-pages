### Download the engine

To play a Solarus quest, you need **Solarus Launcher** (or, at least, the **engine**). Please make sure you have [installed it first](/en/solarus/download).

If you already have Solarus Launcher, you can skip to **step 2**.

### Download the quest

Click on the following button to download the quest as a **.solarus file**. You can then open it from **Solarus Launcher** (or from the terminal).
