### Synopsis

Link's newest quest is set a few generations after Twilight Princess and spans two different continents of Hyrule. Explore eight unique temples and defeat enemies and bosses in order to gain powerful items to assist you in your goal. Explore many areas of Hyrule and assist different races in their conflicts.

More info on [the official website](https://sites.google.com/site/zeldabom/).

![Book of Mudora](artworks/artwork_book.png "Book of Mudora")
