### Presentation

*The Legend of Zelda: A Link to the Dream* is a a remake of the Game Boy cult classic *The Legend of Zelda: Link's Awakening*, first published in 1993 and enhanced with colors in 1998 with its *DX* version.

![Link](artworks/artwork_link.png "Link")

The Solarus-made remake uses *A Link to the Past* graphics, extended with lots of custom sprites and tiles, in order to represent all the game's characters, enemies and landscapes. Even the music has been remade in SNES-like fashion, to get the perfect feeling as if the game was released on Super Nintendo.

*A Link to the Dream* is the ultimate love letter to the legendary pocket adventure. Koholint has never been such a joy to explore!

![The mysterious owl](artworks/artwork_owl.png "The mysterious owl")

### Synopsis

Link was navigating on his boat when suddenly, a huge storm happened. Thunder was rumbling and the ocean was raging. A thunderbolt broke the mast, and destroyed the boat. Link lost consciousness.

He ended up on the beach of an unknown island, still alive but fainted and wounded. A young girl named Marin took him in and looked after him until he finally woke up. She explained him that he was on Kohlint island, a tropical island in the middle of the ocean. Link also soon met Tarin, her father, who found Link's shield on the beach.

![Marin](artworks/artwork_marin.png "Marin")

Who are these people? Where is Link? And why is this island both so strange and so familiar? And what is this giant egg at the top of the mountain? You'll discover the truth by guiding Link in this epic and oniric quest.

![Tarin](artworks/artwork_tarin.png "Tarin")
