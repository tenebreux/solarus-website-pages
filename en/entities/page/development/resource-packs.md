[container]

# {title}

[row]
[column width="3"]
[/column]
[column width="9"]
[entity-search text-placeholder="Search for"]
[/column]
[/row]

[space]

[row]

<!--Filters-->
[column width="3"]
[box-highlight]
[entity-filter entity-type="resource-pack" filter="licenses" title="License"]
[/box-highlight]
[/column]

<!--Games -->
[column width="9"]
[resource-pack-listing]
[/column]

[/row]

[/container]