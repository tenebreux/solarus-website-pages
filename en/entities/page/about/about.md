[container]

# {title}

[row]
[column]

[button-highlight type="primary" icon="question" url="about/faq" label="Frequently Asked Questions"]

[/column]
[column]

[button-highlight type="primary" icon="team" url="about/contributors" label="Team"]

[/column]
[column]

[button-highlight type="primary" icon="foundation" url="about/nonprofit-organization" label="Nonprofit Organization"]

[/column]
[/row]

[row]
[column]

[button-highlight type="primary" icon="info" url="about/legal" label="Legal Information"]

[/column]
[column]

[button-highlight type="primary" icon="mail" url="about/contact" label="Contact"]

[/column]
[column]
[/column]
[/row]

[/container]