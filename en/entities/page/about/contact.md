[container]

# {title}

[row]

[column]

Before contacting us, please see if your question is not already answered in the [Frequently Asked Questions](/about/faq)!

The [Solarus Forum](http://forum.solarus-games.org/) and the [Solarus Chat](https://discord.gg/yYHjJHt) are the recommended sources of discussions. You will usually get better answers there because the whole Solarus community can try to help. If you want to get more information, give a suggestion or propose a contribution, these are the right places.

However, you can also contact us directly here if you have a specific question or if you need to contact us in private. Do not forget to enter a valid e-mail address so that we can reply to you!

[/column]
[column]
[form-contact subject-label="Subject" message-label="Message" name-label="Name" email-label="Email" button-label="Send" captcha-sitekey="6LfDXZUUAAAAAF90gLsyZSWDZwv605BU6c24XJMv" message-success="The message has been sent. We will answer you as soon as possible." message-error="The message could not be sent because some fields are invalid." to-name="Christopho" to-email="christopho@solarus-games.org"]

[/column]

[/row]

[/container]
