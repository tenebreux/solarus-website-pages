The project is making good progress these days, though it was hard for me during the last few months to find much time to work on Solarus, for professional reasons (I am finishing my phd thesis) and personal ones (I just got married!).

If you are interested in how a quest is built, I just published some detailed documentation about the different <a href="http://www.solarus-engine.org/doc/quest.html">data files that compose a quest</a>. This documentation specifies the format of each file:
<ul>
	<li><a href="http://www.solarus-engine.org/doc/map_syntax.html">Maps</a></li>
	<li><a href="http://www.solarus-engine.org/doc/tileset_syntax.html">Tilesets</a></li>
	<li><a href="http://www.solarus-engine.org/doc/sprite_syntax.html">Sprites</a></li>
	<li><a href="http://www.solarus-engine.org/doc/dialog_syntax.html">Dialog and texts</a></li>
	<li><a href="http://www.solarus-engine.org/doc/dungeon_syntax.html">Dungeons</a></li>
	<li><a href="http://www.solarus-engine.org/doc/lua_api_specification.html">Lua scripting API</a></li>
</ul>