We are proud to announce the release of a new game ! It is called <em><strong>Return of the Hylian - Solarus Edition</strong></em>. However, it is not really a new game, since it is a remake of another fangame originally made by Vincent Jouillat from 2009. This time, the game has been completely remade with Solarus, thus featuring many improvements thanks to the engine.<a href="http://www.solarus-games.org/wp-content/uploads/2015/08/roth_se_logo.png"><img class=" wp-image-1150 size-medium aligncenter" src="/images/roth_se_logo-300x201.png" alt="roth_se_logo" width="300" height="201" /></a>

If you've never played the original version, you will notice that the game's style and flow is rather different than our original creations : no tangled labyrinths, no hair-pulling-out enigmas, and less sinous dungeons rooms. It is a more accessible and relatively short game, but remember that <em>Return of the Hylian</em> is actually the first iteration of a quadrology of excellent games.

This first version is not perfect, and it is possible that you still encounter some bugs. Don't hesitate to notify us, and it will be corrected as soon as possible.

What is missing in this 1.0.0 version, and will be added in the next weeks :
<ul>
	<li>Configurable controls</li>
	<li>English translation (for the moment, the only available language is French, but English is coming soon)</li>
	<li>More improvements everywhere</li>
</ul>
You can find more information and download the game on <a href="http://www.solarus-games.org/games/zelda-return-of-the-hylian-se/">its dedicated page</a>.

Have fun !

<a href="http://www.solarus-games.org/wp-content/uploads/2015/08/link_roth_encrage_2.jpg"><img class="aligncenter wp-image-1183 size-medium" src="/images/link_roth_encrage_2-300x300.jpg" alt="link_roth_encrage_2" width="300" height="300" /></a>