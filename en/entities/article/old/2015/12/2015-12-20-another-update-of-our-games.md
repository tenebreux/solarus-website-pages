Here is a small update of our games! Not a huge one, but it fixes the Italian translation of ZSDX and a few other issues.
<h2>Changes in Zelda ROTH SE 1.0.8</h2>
<ul>
	<li>Increase rupee drop rates (#86).</li>
	<li>Increase chances of getting bombs before dungeon 1.</li>
	<li>Fix inappropriate English translation.</li>
</ul>
<h2>Changes in ZSDX 1.10.3</h2>
<ul>
	<li>Fix Italian dialogs exceeding the dialog box (thanks Marco).</li>
	<li>Fix game-over stopped sometimes using a workaround (#87).</li>
</ul>
<h2>Changes in ZSXD 1.10.3</h2>
<ul>
	<li>Fix game-over stopped sometimes using a workaround.</li>
	<li>Fix dialog cursor drawn at wrong position after successive questions.</li>
</ul>
Enjoy!