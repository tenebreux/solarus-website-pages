The Solarus resource pack of Legend of Zelda: A Link to the Past is regularly updated even if I don't always annouce it. Lots of elements are still missing (most importantly, enemies!) but every update improves it step by step!

Today I just added a lot of non-playing character sprites. There are now more than 50 NPC sprites available! I believe we now have most character sprites from Legend of Zelda: A Link to the Past.
<ul>
 	<li>Download the latest <a href="https://github.com/christopho/solarus-alttp-pack/releases">ALTTP resource pack</a></li>
</ul>
<a href="/images/alttp.png"><img class="aligncenter" src="/images/alttp.png" width="512" height="512" /></a>

I use this resource pack a lot in my Solarus video tutorials. If you did not know yet, I made a brand new tutorial playlist because the old one was a bit outdated. The new tutorial playlist is updated every Saturday with two videos!
<ul>
 	<li><a href="https://www.youtube.com/playlist?list=PLzJ4jb-Y0ufxwkj7IlfURcvCxaDCSecJY">Solarus video tutorials</a></li>
</ul>
Follow me on Twitter (<a href="https://twitter.com/ChristophoZS">@ChristophoZS</a>) to know about new tutorials and get more news about Solarus. Feel free to ask on the <a href="http://forum.solarus-games.org">Solarus forums</a> if you have any question when updating your quest or following a tutorial!