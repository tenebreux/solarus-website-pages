This is a very important new feature of Solarus 1.1. The dialog box system is now fully scriptable using the Lua API! It means that you can now make any feature you want to your dialogs, like changing the font, the color, the text alignment, adding images or sprites, making questions with multiple possible choices�

The <a href="http://www.solarus-games.org/doc/1.1/quest_language_dialogs.html">syntax of the dialog files</a>�does not change, expect that any custom property is allowed in a dialog. Actually, only the dialog id and the text are mandatory. Everything else (the icon, whether there is a question, whether the dialog can be skipped�) is now seen as additional, custom properties of your quest.

If you don't make a scripted dialog box in Lua, the engine shows a minimal dialog box without decoration. But it is recommended to make your own one!

Here is an example of a small map script with a non-playing character that shows a simple dialog when the hero talks to him:
<pre>local map = ...

function some_npc:on_interaction()
  -- Remember that you specify a dialog id, not directly the text to show.
  -- The text is defined in the dialogs.dat file of the current language.
  map:get_game():start_dialog("welcome_to_my_house")
end</pre>
Here is a more complex example, with an NPC that asks a question. This example assumes that your dialog box system can ask questions to the player, and returns the answer as a boolean value.
<pre>local map = ...
local game = map:get_game()

function another_npc:on_interaction()
  game:start_dialog("give_me_100_rupees_please", function(answer)
    if answer then
      if game:get_money() &gt;= 100 then
        game:remove_money(100)
        game:start_dialog("thanks")
      else
        sol.audio.play_sound("wrong")
        game:start_dialog("not_enough_money")
      end
    else
      game:start_dialog("not_happy")
    end
  end)
end</pre>
The new dialog API is very flexible. When you show a dialog, you can pass a parameter to your custom dialog box system (this feature is not used in this example), and symmetrically, your custom dialog box system can return a result (like the answer selected by the player in this example).

Fore more details, see the documentation of <a href="http://www.solarus-games.org/doc/1.1/lua_api_game.html#lua_api_game_start_dialog">game:start_dialog()</a> in the Solarus Lua API.