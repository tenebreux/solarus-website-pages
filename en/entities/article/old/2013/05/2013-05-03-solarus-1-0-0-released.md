The "I love Lua" release.

<a href="http://www.solarus-games.org/wp-content/uploads/2013/05/solarus-logo-black-on-transparent.png"><img class="aligncenter size-medium wp-image-377" title="Solarus logo" src="/images/solarus-logo-black-on-transparent-300x90.png" alt="" width="300" height="90" /></a>

Good news! After a year and a half of hard work, the engine is now considered to be usable to make your own games.
<div>This is a major release. The version number switches from 0.x to 1.x because there is a brand new Lua scripting API.</div>
I now consider that Solarus can be used to create your own Zelda-like games in decent conditions.
By "in decent conditions", I essentially mean: with a clean and stable scripting API. Clean because there are nice datatypes now and the API is much easier to use, less error-prone and more with the Lua spirit. Stable because future versions of the scripting API will now try to keep compatibility with existing scripts. Any API change will now be clearly documented on this website and in the change log.

Data files and scripts written for solarus 0.x are not compatible with solarus 1.0. I don't think many people have 0.x quests, but conversion scripts are provided to upgrade your existing data files if it is the case.
Scripts cannot be upgraded automatically though, because the scripting API, which was working but ugly and unstable, has totally changed.

The quest editor celebrates its very first release. It is not full-featured yet (some files still need to be edited by hand), but at least it works now. You can create a quest, manage its data files and edit maps and tilesets.
<ul>
	<li><a title="Download Solarus" href="http://www.solarus-games.org/downloads/download-solarus/">Download Solarus 1.0.0</a></li>
	<li><a title="Solarus Quest Editor" href="http://www.solarus-games.org/solarus/quest-editor/">Solarus Quest Editor</a></li>
	<li><a href="http://www.solarus-games.org/doc/1.0.0/lua_api.html">Lua API documentation</a></li>
</ul>
<h2>Changes</h2>
<ul>
	<li>Rewrite the Lua scripting API from scratch. Cleaner, real datatypes, much easier to use, less error-prone, much more features, fully documentated.</li>
	<li>All scripts now live in a single Lua world.</li>
	<li>Add support of scripted graphics.</li>
	<li>Add support of scripted menus. Menus like the title screen, the savegames menu, the pause menu and the HUD are no longer hardcoded into the engine.</li>
	<li>Doors, chests, teletransporters and destinations are now much more flexible and customizable.</li>
	<li>Except tiles, all map entities can now have a name. The name is now optional.</li>
	<li>Change the format of some data files, including maps and tilesets.</li>
	<li>No more ini data files (removed the dependency to SimpleIni).</li>
	<li>Add conversion scripts to upgrade existing data files (but not scripts).</li>
	<li>Fix infinite explosions of bomb flowers.</li>
</ul>
This version also include changes from Solarus 0.9.3, though 0.9.3 is not released yet:
<ul>
	<li>The game screen size can now be set at compilation time.</li>
	<li>Change the savegames directory on Mac OS X.</li>
	<li>Improve the support of Mac OS X, Pandora, Caanoo and other platforms.</li>
	<li>Fix the compilation with Visual C++.</li>
	<li>Fix blocks making sometimes only a half move (#33).</li>
	<li>Fix pixel-precise collisions not always correct (#53).</li>
	<li>Fix the end of target movement on slow machines (#34).</li>
	<li>Fix the hero being freezed when using the hookshot on bomb flowers (#119).</li>
</ul>
<h2>About release numbers</h2>
As of now, Solarus version numbers have the form x.y.z where x is the major version, y is the minor version and z is the patch version.
<ul>
	<li>Patch versions will only provide bug fixes. Therefore, you can safely upgrade. Quest files that you create with the 1.0 format will be compatible with any 1.0.x version of the engine and the editor.</li>
	<li>Minor and major versions provide new features and may change the format of data files and the Lua API. Conversion tools will be provided to upgrade your quest data files, but take care of the Lua API changes.</li>
</ul>
Infomration about new versions will be published here with the detailed changes.
<h2>Our games</h2>
A work is in progress to upgrade our games ZSDX and ZSXD for Solarus 1.0. It is almost finished for ZSDX (it essentially needs testing and bug fixing). This gives you an example of full quest that use the new Lua API, notably with a scripted title screen, a scripted pause menu. The work is done in <a href="https://github.com/christopho/zsdx/tree/v1.6/">branch v1.6 of the ZSDX repository</a>.

In the meantime, we are also preparing a Solarus 0.9.3 version (with the old Lua API, yes!), in order to release as soon as possible new versions of ZSDX and ZSXD that include the bug fixes mentioned above and the German translation of ZSDX.