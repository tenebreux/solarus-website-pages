We are proud to announce a new version of our main creation <a title="Zelda Mystery of Solarus DX" href="http://www.solarus-games.org/games/zelda-mystery-of-solarus-dx/">Zelda Mystery of Solarus DX</a>. This is version 1.5.0, and the quest is now available in English!

<img class="aligncenter" title="Zelda Mystery of Solarus DX screenshot" src="/images/dungeon_8_prickles.png" alt="" width="320" height="240" />
<p style="text-align: center;"><a href="http://www.zelda-solarus.com/jeu-zsdx-download&amp;lang=en"><strong>Download now</strong></a> on Zelda Solarus</p>
Big thanks to the <a href="http://boards.openpandora.org/index.php?/topic/6462-translation-help-needed-for-zelda-solarus-dx/">OpenPandora</a> community who initiated the translation work, and to Jeff, Rypervenche and AleX_XelA who completed it. The translation was a lot of work, with a text file of 6500 lines of dialogs to translate!

We hope that you will enjoy the game. This is the first English-speaking release of the game. Don't hesitate to tell us if you find a typo or a strange dialog: I will publish updates. Translators did a great work, but nothing is perfect! The best way to report errors in dialogs or make some remarks or suggestions is to file an issue on our <a href="https://github.com/christopho/solarus/issues">github</a>.

Zelda Mystery of Solarus DX 1.5.0 changes:
<ul>
	<li>New language available: English</li>
	<li>Fix two minor issues in dungeon 5</li>
</ul>
<div>Solarus 0.9.2 changes:</div>
<div>
<ul>
	<li>Fix two bugs with teletransporters</li>
</ul>
</div>
Full changelogs (including previous versionss) of both projects are in the git repository. As of now, future updates will be mentioned on this page.

The website also has a <a title="Downloads" href="http://www.solarus-games.org/games/downloads/">downloads page</a> now. I have also improved the top navigation bar as well as the right menus. The right menus now provide direct access to our games and to useful links for developers. It's way better than before, but I'm not a an expert and your <a title="Contact" href="http://www.solarus-games.org/contact/">feedback</a> is welcome ;)