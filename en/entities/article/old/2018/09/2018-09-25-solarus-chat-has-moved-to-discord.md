<a href="https://discord.gg/a32R7GR"><img class="aligncenter wp-image-1822 size-medium" src="/images/Discord-LogoWordmark-Color-300x102.png" alt="" width="300" height="102" /></a>

We were previously using Gitter to chat. However, it has multiple issues so we decided to move to Discord. You can now <a href="https://discord.gg/yYHjJHt">join the server</a> to chat with us, ask questions, and help development.

&nbsp;