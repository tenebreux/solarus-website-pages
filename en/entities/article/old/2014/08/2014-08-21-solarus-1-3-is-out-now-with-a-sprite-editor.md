UPDATE: a bugfix version 1.3.1 was just released. It fixes an error when opening a newly created sprite, and improves the sorting of resources with numbers in their id in the quest tree.

A new release of Solarus is available: version 1.3!

The main improvement of Solarus 1.3 is the graphical sprite editor integrated in Solarus Quest Editor. You will no longer have to edit sprite sheet data files by hand!

[caption id="attachment_910" align="aligncenter" width="720"]<a href="http://www.solarus-games.org/wp-content/uploads/2014/08/sprite_editor.png"><img class="wp-image-910 size-large" src="/images/sprite_editor-1024x576.png" alt="Sprite Editor" width="720" height="405" /></a> Screenshot of Solarus Quest Editor 1.3, modifying the sprite of an enemy in ZSDX.[/caption]

The sprite editor was primarily developed by Maxs. You can create and edit sprite sheets. A tree shows the hierarchy of animations and direction of your sprite. And below the tree, you can edit the animation and direction that are selected in the tree.

The main changes of this release are in the quest editor. Another important improvement is that tile patterns can now be identified by a string instead of auto-generated integers. This allows to more easily maintain different similar tilesets.

There are also a few new features in the engine and in the Lua API, like more customizable switches, but nothing that breaks compatibility of your scripts.
<ul>
	<li><a title="Download Solarus" href="http://www.solarus-games.org/download/">Download Solarus 1.3</a></li>
	<li><a title="Solarus Quest Editor" href="http://www.solarus-games.org/development/quest-editor/">Solarus Quest Editor</a></li>
	<li><a title="LUA API documentation" href="http://www.solarus-games.org/doc/latest/lua_api.html">Lua API documentation</a></li>
	<li><a href="http://wiki.solarus-games.org/doku.php?id=migration_guide">Migration guide</a></li>
</ul>
As always, our games ZSDX and ZSXD were also upgraded to give you up-to-date examples of games. In ZSDX, there is also a new world minimap made by Neovyse.
<ul>
	<li><a title="Download ZSDX" href="http://www.solarus-games.org/games/zelda-mystery-of-solarus-dx/">Download Zelda Mystery of Solarus DX 1.9</a></li>
	<li><a title="Download ZSXD" href="http://www.solarus-games.org/games/zelda-mystery-of-solarus-xd/">Download Zelda Mystery of Solarus XD 1.9</a></li>
</ul>
Here is the full changelog.
<h2>Changes in Solarus 1.3</h2>
Lua API changes that do not introduce incompatibilities:
<ul>
	<li>Add mouse functions and events (experimental, more will come in Solarus 1.4).</li>
	<li>Add a method sprite:get_animation_set_id() (#552).</li>
	<li>Add a method sprite:has_animation() (#525).</li>
	<li>Add a method sprite:get_num_directions().</li>
	<li>Add a method hero:get_solid_ground_position() (#572).</li>
	<li>Add a method switch:get_sprite().</li>
	<li>Allow to <strong>customize the sprite and sound of switches</strong> (#553).</li>
	<li>Add a method enemy:get_treasure() (#501).</li>
</ul>
Bug fixes:
<ul>
	<li>Fix the write directory not having priority over the data dir since 1.1.</li>
	<li>Fix pickable/destructible:get_treasure() returning wrong types.</li>
	<li>Fix custom entity collision detection when the other is not moving (#551).</li>
	<li>Allow to call map methods even when the map is not running.</li>
</ul>
<h2>Changes in Solarus Quest Editor 1.3</h2>
New features:
<ul>
	<li>Add a <strong>sprite editor</strong> (#135). By Maxs.</li>
	<li>Add a zoom level of 400%. By Maxs.</li>
	<li>Add keyboard/mouse zoom features to sprites and tilesets. By Maxs.</li>
	<li>Add <strong>Lua syntax coloring</strong> (#470). By Maxs.</li>
	<li>Add a close button on tabs (#439). By Maxs.</li>
	<li>Rework the <strong>quest tree</strong> to show the file hierarchy and Lua scripts. By Maxs.</li>
	<li>Add specific icons for each resource type in the quest tree.</li>
	<li>Move the entity checkboxes to the map view settings panel. By Maxs.</li>
	<li>Allow to <strong>change the id of a tile pattern</strong> in the tileset editor (#559).</li>
	<li>Don't initially maximize the editor window.</li>
</ul>
Bug fixes:
<ul>
	<li>Fix converting quests to several versions in one go.</li>
</ul>
<h2>Incompatibilities</h2>
These improvements involve changes that introduce slight incompatibilities in the format of data files, but no incompatibility in the Lua API. Make a backup, and then see the <a href="http://wiki.solarus-games.org/doku.php?id=migration_guide">migration guide</a> on the wiki to know how to upgrade.
<h2>Changes in Zelda Mystery of Solarus DX 1.9</h2>
New features:
<ul>
	<li>
<div id="LC4" class="line">New world minimap. By Neovyse.</div></li>
	<li>
<div id="LC4" class="line">Make cmake paths more modifiable. By hasufell.</div></li>
</ul>
<p id="LC4" class="line">Bug fixes:</p>

<ul>
	<li>Fix direction of vertical movement on joypad in menus (#85). By xethm55.</li>
	<li>Clarify license of some files.</li>
</ul>
<h2>Changes in Zelda Mystery of Solarus XD 1.9</h2>
New features:
<ul>
	<li>
<div id="LC7" class="line">Make cmake paths more modifiable. By hasufell.</div></li>
</ul>
<p id="LC7" class="line">Bug fixes:</p>

<ul>
	<li>
<div id="LC8" class="line">Fix direction of vertical movement on joypad in menus.</div></li>
	<li>
<div id="LC7" class="line">Clarify the license of some files.</div>
</li>
</ul>