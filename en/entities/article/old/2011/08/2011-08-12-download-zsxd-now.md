Our parodic creation Zelda Mystery of Solarus XD is now available in English, as announced in my previous post. You can find it on the <a href="http://www.zelda-solarus.com/jeu-zsxd-download&amp;lang=en">download page of ZSXD</a> on Zelda Solarus (the website is in French but the download page exists in English). You can also download it directly with one of the links below:
<ul>
	<li>Windows: <a href="http://www.zelda-solarus.com/download-zsxd_win32_setup">installer</a> or <a href="http://www.zelda-solarus.com/download-zsxd_win32_zip">zip file</a></li>
	<li>Debian or Ubuntu : <a href="http://www.zelda-solarus.com/download-zsxd_debian-i386">32 bit</a> or <a href="http://www.zelda-solarus.com/download-zsxd_debian-amd64">64 bit</a> (to play, the command to run is "Solarus")</li>
	<li><a href="http://www.zelda-solarus.com/download-zsxd_src">Source code</a></li>
</ul>
Have fun! But don't forget: this game is essentially a big April 1st joke ;)