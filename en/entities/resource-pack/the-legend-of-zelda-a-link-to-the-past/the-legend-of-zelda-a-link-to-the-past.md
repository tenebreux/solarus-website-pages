### Presentation

*The Legend of Zelda: A Link to the Past*, released in 1991 on Super Nintendo, is the original reason Solarus exists. Before being a general-purpose ARPG game engine, Solarus was first an ALTTP game engine.

This resource pack has been gathered progressively, by creating fan-games inspired by this legendary game, and today it contains almost all the necessary elements to rebuild *A Link to the Past* with Solarus, from maps to NPCs, and from menus to items.

![A Link to the Past box](images/alttp_box.png)
