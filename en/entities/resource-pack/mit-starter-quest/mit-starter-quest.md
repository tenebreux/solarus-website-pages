### Presentation

This resource pack contains only free elements totally free to use, in the **public domain**, unlike [Free Resource Pack](/en/ressource-pack/free-resource-pack) under GPL license.

It allows you to create a proprietary commercial game without having the legal obligation to make your game open-source.
