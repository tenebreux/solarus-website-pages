# {title}

[youtube id="KyF4LB1YOSY"]

## Summary

- Interacting with an NPC entity
- Creating an NPC dialog with a question
- Creating a map script to manage NPC interactions and display the corresponding dialogs
- Setting savegame values to remember the state of NPC interactions

## Resources

- Video made with Solarus 1.5.
- [Download Solarus](/solarus/download)
- [Solarus documentation](https://www.solarus-games.org/doc/latest/)
- [How to program in Lua](http://www.lua.org/pil/contents.html)
- [Legend of Zelda: A Link to the Past resource pack](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)
