# {title}

[youtube id="aoMXvvMyk1k"]

## Summary

- Creating the flippers item
- Creating the dialog for the flippers
- Working with the `swim` ability
- Making the hero jump into deep water (`jump_over_water` ability)

## Resources

- Video made with Solarus 1.5.
- [Download Solarus](/solarus/download)
- [Solarus documentation](https://www.solarus-games.org/doc/latest/)
- [How to program in Lua](http://www.lua.org/pil/contents.html)
- [Legend of Zelda: A Link to the Past resource pack](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)
