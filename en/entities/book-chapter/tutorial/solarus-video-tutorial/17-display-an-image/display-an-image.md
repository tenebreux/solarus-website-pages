# {title}

[youtube id="5Jsh43NzHdw"]

## Summary

- Using `map:on_draw()` to display an image on the screen
- Creating a surface from an image
- Using `sol.main:on_draw()` to display an image on the screen globally
- How to create a title screen menu that displays an image
  - Stopping the menu when a key is pressed

## Resources

- Video made with Solarus 1.5.
- [Download Solarus](/solarus/download)
- [Solarus documentation](https://www.solarus-games.org/doc/latest/)
- [How to program in Lua](http://www.lua.org/pil/contents.html)
- [Legend of Zelda: A Link to the Past resource pack](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)
