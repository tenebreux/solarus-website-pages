# {title}

[youtube id="3aLyAJfhHzc"]

## Summary

- Difference between usual NPCs (somebody) and generalized NPCs (something)
- Creating a sign that displays a dialog
- Interacting with an NPC from across a counter
  - Using a map script to change the NPC direction on interaction

## Resources

- Video made with Solarus 1.5.
- [Download Solarus](/solarus/download)
- [Solarus documentation](https://www.solarus-games.org/doc/latest/)
- [How to program in Lua](http://www.lua.org/pil/contents.html)
- [Legend of Zelda: A Link to the Past resource pack](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)
- [Useful resources to reproduce this chapter](https://www.solarus-games.org/tuto/en/basics/ep19_resources.zip)
