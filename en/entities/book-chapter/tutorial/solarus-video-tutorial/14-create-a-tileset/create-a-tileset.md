# {title}

[youtube id="bC0IScIohjA"]

## Summary

- How to create the tileset image in [GIMP](https://www.gimp.org/), using *Legend of Zelda: Link's Awakening* Tail Cave graphics
- Using the tileset editor
- Changing the tileset of an existing map (pattern naming)

## Resources

- Video made with Solarus 1.5.
- [Download Solarus](/solarus/download)
- [Solarus documentation](https://www.solarus-games.org/doc/latest/)
- [How to program in Lua](http://www.lua.org/pil/contents.html)
- [Legend of Zelda: A Link to the Past resource pack](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)
