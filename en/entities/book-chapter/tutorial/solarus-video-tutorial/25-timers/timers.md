# {title}

[youtube id="RIjs7cHZe-0"]

## Summary

- Working with timers to add a delay in a script
  - Playing a sound 5 seconds after activating a switch
- Timer contexts
- Repeating a timer
  - Forever
  - Only 3 times
- Suspending a timer
- Draining water gradually after switch is activated

## Resources

- Video made with Solarus 1.5.
- [Download Solarus](/solarus/download)
- [Solarus documentation](https://www.solarus-games.org/doc/latest/)
- [How to program in Lua](http://www.lua.org/pil/contents.html)
- [Legend of Zelda: A Link to the Past resource pack](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)
