# {title}

[youtube id="zH2nzQemzRs"]

## Summary

- Creating the rupee item script
  - Setting item shadows
  - Playing a sound when picked
- Creating the rupee treasure dialog
- Difference between `on_started()` and `on_pickable_created()` item events
- Working with item variants

## Resources

- Video made with Solarus 1.5.
- [Download Solarus](/solarus/download)
- [Solarus documentation](https://www.solarus-games.org/doc/latest/)
- [How to program in Lua](http://www.lua.org/pil/contents.html)
- [Legend of Zelda: A Link to the Past resource pack](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)
