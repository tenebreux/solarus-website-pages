# World Map

Here is a generic view of the outside world of the game. You will find here the list of enemies in each zones and the availables locations.

## West Mount Terror (A1)

![Zone map](img/world/a1-west-mount-terror.png)

### Enemies

* Blue Dogman x3
* Red Dogman x2

### Places

1. West Cavern Entrance - 1F
2. West Cavern Exit - 2F (West exit)
3. West Cavern Exit - 1F (East exit)
4. Crystal Temple (Level 7)
5. West Cavern Exit - 2F (East exit)
6. Lasers Cave
7. West Cavern Entrance - 3F
8. West Cavern Exit - 3F

## East Mount Terror (B1)

![Zone map](img/world/b1-east-mount-terror.png)

### Enemies

* Blue Dogman x2
* Red Dogman x2

### Places

1. North Fairy Fountain
2. East Cavern Exit - 4F
3. East Cavern Exit - 5F
4. Optical Illusion Cavern
5. Optical Illusion Cavern Exit
6. Rock Peaks Dungeon (Level 8)

## Ancient Castle (A2)

![Zone map](img/world/a2-ancient-castle.png)

### Enemies

* Blue Knight x2
* Red Knight x1

### Places

1. Ancient Castle Main Entrance (Level 5)
2. Jail Entrance
3. Waterfall Hideout Entrance
4. Ancient Castle Roof Exit
5. Ancient Castle West Wing Exit
6. Ancient Castle East Wing Entrance
7. True Hero Labyrinth Entrance
8. True Hero Labyrinth Exit

## Witch's Hut (B2)

![Zone map](img/world/b2-witch-hut.png)

### Enemies

* Blue Knight x2
* Red Knight x1

### Places

1. River Labyrinth Entrance
2. West Channel Entrance     Entrée Ouest du Canal
3. East Channel Entrance     Entrée Est du Canal
4. Witch's Hut
5. Inferno's Daedalus (Level 6)
6. Bazaar
7. Village's Fairy Fountain Exit
8. Village's Fairy Fountain Entrance - 2F
9. Waterfall Tunnel
10. East Cavern Entrance - 1F
11. East Cavern Exit - 2F
12. East Cavern Exit - 3F
13. East Cavern Entrance - 2F

## West Lyriann (A3)

![Zone map](img/world/a3-west-lyriann.png)

### Enemies

* Soldier x3
* Green Knight x2
* Blue Knight x1
* Red Knight x1

### Places

1. Link's House
2. Store / Armory
3. Sahasrahla's House
4. Bakery
5. Blacksmith's Cave
6. Hole and Flower Cave
7. Telepatic Closet
8. Roc's Cavern (Level 2)
9. Bomb Cave
10. Master Arbror's Den (Level 3)
11. Master Arbror's Den Secret Entrance

## East Lyriann (B3)

![Zone map](img/world/b3-east-lyriann.png)

### Enemies

* Soldier x2
* Green Knight x2
* Blue Knight x1
* Red Knight x1

### Places

1. Rupees House
2. Granny Lyly's House
3. Lyriann Cave
4. Fairy Foutain of Lyriann
5. Bone Key Cave
6. Lost Woods Cave
7. Chests Cave
8. Waterfall Tunnel

## Skyward Tower (C3)

![Zone map](img/world/c3-skyward-tower.png)

### Enemies

* Ropa x2
* Snapdragon x3

### Places

1. Skyward Tower (Secret Level)

## South Forest (A4)

![Zone map](img/world/a4-south-forest.png)

### Enemies

* Soldier x4
* Green Knight x1
* Blue Knight x1

### Places

1. Forest Dungeon (Level 1)
2. Secret exit of Forest Dungeon
3. Twin Caves
4. Twin Caves
5. To Beaumont's Palace (Level 4)
6. Lake West Cave Entrance
7. Lake West Cave Exit
8. Teleport to the Shrine of Memories (Level 9)
9. West Exit Shrine of Memories - 2F
10. East Exit Shrine of Memories - 2F

## Lake (B4)

![Zone map](img/world/b4-lake.png)

### Enemies

* Green Knight x1
* Blue Knight x1
* Red Knight x1

### Places

1. Lake's Shop
2. Lake's Shop Secret Exit
3. Surprise Wall
4. Billy the Reckless's Den
5. Cave under the Waterfall Hidden Exit
6. Cave under the Waterfall Exit
7. Lake East Cave
