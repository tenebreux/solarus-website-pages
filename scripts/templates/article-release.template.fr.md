Nous avons tout juste publié une version de maintenance pour Solarus, qui passe ainsi en version **%(version)s**.

## Changelog

Voici la liste exhaustive de tous les changements inclus dans la version %(version)s de Solarus. Merci à tous les contributeurs.

%(content)s
