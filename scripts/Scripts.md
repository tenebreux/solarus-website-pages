# Scripts

- [Update Solarus version](#update-solarus-version)
- [Batch add/remove data for JSON files](#batch-add-or-remove-data-for-json-files)

## Update Solarus version

Updating Solarus version can be cumbersome. There is a quick and dirty Python script `update-solarus-version.py` to update all the necessary files automatically.

Example:

```bash
python scripts/update-solarus-version.py -version "1.6.4" -author "Christopho"
```

It will:

- Bump the **version number on the homepage** (English and French)
- Add a **news article** with today's date (if no specified), containing all the changelogs for all the projects (if a version matching the new one is found in the files). Article thumbnail is also generated.
- Update the **Windows package** download link.

Use `--help` to get more help about the options. 

## Batch add or remove data for JSON files

Allows to walk all the files recursively and edit the JSON files to add/remove/edit a key/value pair.

Note that without `-o` or `--override`, the script `json-add-property.py` won't replace the existing key/value pair if it is already present in the JSON object.

Example:

```bash
python scripts/json-add-property.py -root "fr/entities/article/old/2017" -name "meta_image" -value "assets/images/meta_image_fallback.jpg" -f -v -r
```

Note the `-r` option to ensure **recursiveness**.
